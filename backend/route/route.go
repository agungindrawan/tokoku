package route

import (
	jwtware "github.com/gofiber/contrib/jwt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/indrawanagung/shop_api_golang/controller"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/spf13/viper"
)

func New(
	userController controller.UserControllerInterface,
	cityController controller.CityControllerInterface,
	addressController controller.AddressControllerInterface,
	authController controller.AuthControllerInterface,
	StoreController controller.StoreControllerInterface,
	ProductController controller.ProductControllerInterface,
) *fiber.App {
	authentication := jwtware.New(jwtware.Config{
		SigningKey: jwtware.SigningKey{Key: []byte(viper.GetString("SECRET_KEY"))},
	})
	app := fiber.New(fiber.Config{ErrorHandler: exception.ErrorHandler})
	app.Use(cors.New())
	app.Use(recover.New())
	api := app.Group("/shop-api")
	v1 := api.Group("/v1")

	seller := v1.Group("/seller")
	SellerRouter(seller, authentication, StoreController, ProductController)

	v1.Post("/auth/login", authController.Login)
	v1.Get("/users", authentication, userController.FindByID)
	v1.Post("/users", userController.Save)
	v1.Put("/users", authentication, userController.Update)
	//v1.Delete("/users/:userID", userController.Delete)
	v1.Get("/cities", cityController.FindAll)
	v1.Get("/cities/:cityID", cityController.FindByID)

	v1.Get("/address", authentication, addressController.FindAllByUserID)
	v1.Get("/address/:addressID", authentication, addressController.FindByID)
	v1.Post("/address", authentication, addressController.Save)
	v1.Put("/address/:addressID", authentication, addressController.Update)
	v1.Delete("/address/:addressID", authentication, addressController.Delete)

	v1.Get("/products", ProductController.BuyerFindAllProduct)
	v1.Get("/products/:id", ProductController.BuyerFindByIDProduct)

	v1.Get("/carts", authentication, ProductController.BuyerFindCart)
	v1.Get("/carts/:id", authentication, ProductController.BuyerFindByIDCart)
	v1.Post("/carts", authentication, ProductController.BuyerAddCart)
	v1.Put("/carts/:id", authentication, ProductController.BuyerUpdateCart)
	v1.Delete("/carts/:id", authentication, ProductController.BuyerDeleteCart)

	v1.Post("/orders/submit", authentication, ProductController.BuyerSubmitOrder)
	v1.Get("/orders/confirmed_payment/:id", authentication, ProductController.BuyerOrderConfirmedPayment)
	v1.Get("/orders", authentication, ProductController.BuyerFindAllOrder)

	v1.Get("/payments/type", authentication, ProductController.FindAllPaymentType)

	// Error Handler

	return app
}
