package route

import (
	"github.com/gofiber/fiber/v2"
	"github.com/indrawanagung/shop_api_golang/controller"
)

func SellerRouter(
	router fiber.Router,
	authentication fiber.Handler,
	storeController controller.StoreControllerInterface,
	productController controller.ProductControllerInterface,
) {
	router.Get("/store", authentication, storeController.FindByUserID)
	router.Post("/store", authentication, storeController.SaveOrUpdate)
	router.Post("/warehouses", authentication, storeController.SaveWarehouse)
	router.Get("/warehouses", authentication, storeController.FindAllWarehouse)
	router.Get("/warehouses/:warehouseID", authentication, storeController.FindWarehouseByID)
	router.Put("/warehouses/:warehouseID", authentication, storeController.UpdateWarehouse)

	router.Get("/products/categories", authentication, productController.FindAllProductCategory)
	router.Post("/products", authentication, productController.SellerSaveProduct)
	router.Get("/products", authentication, productController.SellerFindAllProduct)
	router.Get("/products/:id", authentication, productController.SellerFindByIDProduct)
	router.Put("/products/:id", authentication, productController.SellerUpdateProduct)
	router.Delete("/products/:id", authentication, productController.SellerDeleteProduct)
}
