package repository

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2/log"
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"gorm.io/gorm"
)

type ProductRepositoryImpl struct {
}

func NewProductRepository() ProductRepositoryInterface {
	return &ProductRepositoryImpl{}
}

func (p ProductRepositoryImpl) FindAllProductCategory(db *gorm.DB) []domain.ProductCategory {
	var productCategories []domain.ProductCategory
	err := db.Find(&productCategories).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return productCategories
}

func (p ProductRepositoryImpl) SellerSaveProduct(db *gorm.DB, product domain.Product) {
	err := db.Save(&product).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) FindByIDProductCategory(db *gorm.DB, productCategoryID string) (domain.ProductCategory, error) {
	var productCategory domain.ProductCategory
	err := db.First(&productCategory, "id = ?", productCategoryID).Error
	if err != nil {
		if err.Error() != "record not found" {
			log.Error(err)
			panic(err)
		}
		return productCategory, errors.New(fmt.Sprintf("product category id %s is not found", productCategoryID))
	}
	return productCategory, nil
}

func (p ProductRepositoryImpl) SellerFindAllProduct(db *gorm.DB, userID string) []web.SellerProductResponse {
	var products []web.SellerProductResponse
	err := db.Select("products.id, products.name, products.product_category_id, products.price, product_categories.category_name, warehouses.id as warehouse_id, status.name as status_name, product_images.image_url").
		Joins("join warehouses on warehouses.id = products.warehouse_id").
		Joins("join product_categories on product_categories.id = products.product_category_id").
		Joins("join product_images on product_images.product_id = products.id").
		Joins("join stores on stores.id = warehouses.store_id").
		Joins("join status on status.id = products.status_id").Table("products").
		Where("stores.user_id = ?", userID).
		Where("products.deleted_at is null").
		Scan(&products).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return products
}

func (p ProductRepositoryImpl) SaveImageProduct(db *gorm.DB, productImage domain.ProductImage) {
	err := db.Save(&productImage).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) SellerFindByIDProduct(db *gorm.DB, userID string, productID string) (web.SellerProductResponse, error) {
	var product web.SellerProductResponse
	err := db.Select("products.id, products.name, products.product_category_id, product_stocks.total as stock, product_images.image_url, products.price, product_categories.category_name, warehouses.id as warehouse_id, status.name as status_name").
		Joins("join warehouses on warehouses.id = products.warehouse_id").
		Joins("join product_categories on product_categories.id = products.product_category_id").
		Joins("join product_images on product_images.product_id = products.id").
		Joins("join product_stocks on product_stocks.product_id = products.id").
		Joins("join stores on stores.id = warehouses.store_id").
		Joins("join status on status.id = products.status_id").Table("products").
		Where("stores.user_id = ?", userID).
		Where("products.id = ?", productID).
		Scan(&product).Error
	if err != nil {
		if err.Error() != "record not found" {
			log.Error(err)
			panic(err)
		}
		return product, errors.New(fmt.Sprintf("product id %s is not found", productID))
	}
	fmt.Println("product : ", product)
	return product, nil
}

func (p ProductRepositoryImpl) SellerDeleteProduct(db *gorm.DB, productID string) {
	err := db.Delete(&domain.Product{ID: productID}, "id = ?", productID).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	fmt.Println("product id : ", productID)
}

func (p ProductRepositoryImpl) SellerSaveProductStock(db *gorm.DB, productStock domain.ProductStock) {
	err := db.Save(&productStock).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) BuyerFindAllProduct(db *gorm.DB) []web.BuyerProductResponse {
	var products []web.BuyerProductResponse
	err := db.Select("products.id, products.name, products.product_category_id, products.price, product_categories.category_name, warehouses.id as warehouse_id, status.name as status_name, product_images.image_url").
		Joins("join warehouses on warehouses.id = products.warehouse_id").
		Joins("join product_categories on product_categories.id = products.product_category_id").
		Joins("join product_images on product_images.product_id = products.id").
		Joins("join stores on stores.id = warehouses.store_id").
		Joins("join status on status.id = products.status_id").Table("products").
		Where("products.deleted_at is null").
		Scan(&products).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return products
}

func (p ProductRepositoryImpl) BuyerFindByIDProduct(db *gorm.DB, productID string) (web.BuyerProductResponse, error) {
	var product web.BuyerProductResponse
	err := db.Select("products.id, products.name, product_stocks.total as stock, products.product_category_id, products.price, product_categories.category_name, warehouses.id as warehouse_id, status.name as status_name, product_images.image_url").
		Joins("join warehouses on warehouses.id = products.warehouse_id").
		Joins("join product_categories on product_categories.id = products.product_category_id").
		Joins("join product_images on product_images.product_id = products.id").
		Joins("join stores on stores.id = warehouses.store_id").
		Joins("join product_stocks on product_stocks.product_id = products.id").
		Joins("join status on status.id = products.status_id").Table("products").
		Where("products.deleted_at is null").
		Where("products.id = ? ", productID).
		First(&product).Error
	if err != nil {
		log.Error(err)
	}
	if product == (web.BuyerProductResponse{}) {
		return product, errors.New(fmt.Sprintf("product id %s is not found", productID))
	}

	return product, nil
}

func (p ProductRepositoryImpl) BuyerFindCart(db *gorm.DB, userID string) []web.CartResponse {
	var carts []web.CartResponse
	err := db.Select("sci.id , sci.product_id , sci.user_id , sci.qty, p.name as product_name, p.price, pi2.image_url").
		Joins("join products p on p.id = sci.product_id").
		Joins("join users u on u.id = sci.user_id").
		Joins("join product_images pi2 on pi2.product_id = p.id").
		Table("shopping_cart_items sci").
		Where("p.deleted_at is null").
		Where("sci.deleted_at is null").
		Where("u.id = ?", userID).
		Scan(&carts).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return carts
}

func (p ProductRepositoryImpl) BuyerAddCartProduct(db *gorm.DB, cartProduct domain.ShoppingCartItems) {
	var item domain.ShoppingCartItems
	err := db.First(&item, "product_id = ? and user_id = ? ", cartProduct.ProductID, cartProduct.UserID).Error

	if err != nil {
		if err.Error() == "record not found" {
			err = db.Save(&cartProduct).Error
			if err != nil {
				log.Error(err)
				panic(err)
			}
		} else {
			log.Error(err)
			panic(err)
		}
	}
}

func (p ProductRepositoryImpl) BuyerUpdateCartProduct(db *gorm.DB, cartProduct domain.ShoppingCartItems) {
	err := db.Save(&cartProduct).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) BuyerFindByIDCart(db *gorm.DB, userID string, cartID string) (web.CartResponse, error) {
	var cart web.CartResponse
	err := db.Select("sci.id , sci.product_id , sci.created_at,sci.user_id , sci.qty, p.name as product_name, p.price, pi2.image_url").
		Joins("join products p on p.id = sci.product_id").
		Joins("join users u on u.id = sci.user_id").
		Joins("join product_images pi2 on pi2.product_id = p.id").
		Table("shopping_cart_items sci").
		Where("p.deleted_at is null").
		Where("sci.deleted_at is null").
		Where("u.id = ?", userID).
		Where("sci.id = ?", cartID).
		First(&cart).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}

	if cart == (web.CartResponse{}) {
		return cart, errors.New(fmt.Sprintf("cart product id %s is not found", cartID))
	}
	return cart, nil
}

func (p ProductRepositoryImpl) BuyerDeleteCartProduct(db *gorm.DB, userID string, cartID string) {
	err := db.Delete(&domain.ShoppingCartItems{
		ID:     cartID,
		UserID: userID,
	}).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) SubmitOrder(db *gorm.DB, order domain.ShopOrder) {
	err := db.Save(&order).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) SubmitOrderItem(db *gorm.DB, item domain.ShopOrderItem) {
	err := db.Save(&item).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) FindAllPaymentType(db *gorm.DB) []domain.PaymentType {
	var paymentTypes []domain.PaymentType

	err := db.Find(&paymentTypes).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return paymentTypes
}

func (p ProductRepositoryImpl) FindByIDPaymentType(db *gorm.DB, id string) (domain.PaymentType, error) {
	var paymentType domain.PaymentType

	err := db.First(&paymentType, "id = ?", id).Error

	if err != nil {
		if err.Error() == "record not found" {
			return paymentType, errors.New(fmt.Sprintf("payment type id %s is not found", id))
		} else {
			log.Error(err)
			panic(err)
		}
	}
	return paymentType, nil
}

func (p ProductRepositoryImpl) SavePaymentMethod(db *gorm.DB, payment domain.PaymentMethod) {
	err := db.Save(&payment).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (p ProductRepositoryImpl) BuyerFindAllOrder(db *gorm.DB, userID string) []web.BuyerOrderResponse {
	var orders []web.BuyerOrderScan

	err := db.Table("shop_orders so").
		Select("so.id, so.user_id, so.address_id, a.name as address_name, so.total_price, so.ordered_at, so.payment_method_id, so.status_id, status.name as status_name, status.color as status_color").
		Joins("join address a on a.id = so.address_id ").
		Joins("join status status on status.id = so.status_id ").
		Where("so.user_id = ? ", userID).
		Where("so.deleted_at is null").
		Scan(&orders).Error

	if err != nil {
		log.Error(err)
		panic(err)
	}

	var ordersResponse []web.BuyerOrderResponse

	for _, order := range orders {
		items := p.BuyerFindOrderItem(db, userID, order.ID)
		orderResponse := web.BuyerOrderResponse{
			ID:              order.ID,
			UserID:          order.UserID,
			AddressID:       order.AddressID,
			AddressName:     order.AddressName,
			TotalPrice:      order.TotalPrice,
			OrderedAt:       order.OrderedAt,
			PaymentMethodID: order.PaymentMethodID,
			StatusID:        order.StatusID,
			StatusName:      order.StatusName,
			StatusColor:     order.StatusColor,
			Items:           items,
		}
		ordersResponse = append(ordersResponse, orderResponse)
	}

	return ordersResponse
}

func (p ProductRepositoryImpl) BuyerFindOrderItem(db *gorm.DB, userID string, orderID string) []web.BuyerOrderItemResponse {
	var items []web.BuyerOrderItemResponse

	err := db.Raw("SELECT soi.id, p.name as product_name, soi.product_id, soi.shop_order_id, soi.qty, soi.price, pi2.image_url "+
		"FROM public.shop_order_items soi "+
		"join product_images pi2 on pi2.product_id = soi.product_id "+
		"join products p on p.id  = soi.product_id "+
		"join shop_orders so on so.id = soi.shop_order_id "+
		"where soi.shop_order_id = ? and so.user_id = ? ", orderID, userID).Scan(&items).Error

	if err != nil {
		log.Error(err)
		panic(err)
	}
	return items
}

func (p ProductRepositoryImpl) BuyerOrderConfirmedPayment(db *gorm.DB, order domain.ShopOrder) {
	err := db.Exec("UPDATE public.shop_orders "+
		"set status_id=6 "+
		"WHERE id= ? and user_id = ?", order.ID, order.UserID).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	fmt.Println("test")
}

func (p ProductRepositoryImpl) BuyerFindOrderByID(db *gorm.DB, userID string, orderID string) (web.BuyerOrderResponse, error) {
	var order web.BuyerOrderScan

	err := db.Table("shop_orders so").
		Select("so.id, so.user_id, so.address_id, a.name as address_name, so.total_price, so.ordered_at, so.payment_method_id, so.status_id, status.name as status_name").
		Joins("join address a on a.id = so.address_id ").
		Joins("join status status on status.id = so.status_id ").
		Where("so.user_id = ? ", userID).
		Where("so.id = ? ", orderID).
		Where("so.deleted_at is null").
		Scan(&order).Error

	if err != nil {
		log.Error(err)
		panic(err)
	}

	if (order) == (web.BuyerOrderScan{}) {
		return web.BuyerOrderResponse{}, errors.New(fmt.Sprintf("order id %s is not found", orderID))
	}

	var orderResponse web.BuyerOrderResponse

	orderResponse.ID = order.ID
	orderResponse.UserID = order.UserID
	orderResponse.AddressID = order.AddressID
	orderResponse.AddressName = order.AddressName
	orderResponse.TotalPrice = order.TotalPrice
	orderResponse.OrderedAt = order.OrderedAt
	orderResponse.PaymentMethodID = order.PaymentMethodID
	orderResponse.StatusID = order.StatusID
	orderResponse.StatusName = order.StatusName
	items := p.BuyerFindOrderItem(db, userID, order.ID)
	orderResponse.Items = items

	return orderResponse, nil
}
