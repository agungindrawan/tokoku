package repository

import (
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"gorm.io/gorm"
)

type ProductRepositoryInterface interface {
	FindAllProductCategory(db *gorm.DB) []domain.ProductCategory
	FindByIDProductCategory(db *gorm.DB, productID string) (domain.ProductCategory, error)
	SellerSaveProduct(db *gorm.DB, product domain.Product)
	SellerSaveProductStock(db *gorm.DB, productStock domain.ProductStock)
	SaveImageProduct(db *gorm.DB, productImage domain.ProductImage)
	SellerFindAllProduct(db *gorm.DB, userID string) []web.SellerProductResponse
	SellerFindByIDProduct(db *gorm.DB, userID string, productID string) (web.SellerProductResponse, error)
	SellerDeleteProduct(db *gorm.DB, productID string)

	BuyerFindByIDProduct(db *gorm.DB, productID string) (web.BuyerProductResponse, error)
	BuyerFindAllProduct(db *gorm.DB) []web.BuyerProductResponse

	BuyerFindCart(db *gorm.DB, userID string) []web.CartResponse
	BuyerFindByIDCart(db *gorm.DB, userID string, cartID string) (web.CartResponse, error)

	BuyerAddCartProduct(db *gorm.DB, cartProduct domain.ShoppingCartItems)
	BuyerUpdateCartProduct(db *gorm.DB, cartProduct domain.ShoppingCartItems)
	BuyerDeleteCartProduct(db *gorm.DB, userID string, cartID string)

	BuyerFindAllOrder(db *gorm.DB, userID string) []web.BuyerOrderResponse
	BuyerFindOrderByID(db *gorm.DB, userID string, orderID string) (web.BuyerOrderResponse, error)
	BuyerFindOrderItem(db *gorm.DB, userID string, orderID string) []web.BuyerOrderItemResponse
	SubmitOrder(db *gorm.DB, order domain.ShopOrder)
	SubmitOrderItem(db *gorm.DB, item domain.ShopOrderItem)

	SavePaymentMethod(db *gorm.DB, payment domain.PaymentMethod)
	BuyerOrderConfirmedPayment(db *gorm.DB, order domain.ShopOrder)
	FindAllPaymentType(db *gorm.DB) []domain.PaymentType
	FindByIDPaymentType(db *gorm.DB, id string) (domain.PaymentType, error)
}
