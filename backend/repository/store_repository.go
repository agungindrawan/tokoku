package repository

import (
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"gorm.io/gorm"
)

type StoreRepositoryInterface interface {
	FindByUserID(db *gorm.DB, userID string) (domain.Store, error)
	SaveOrUpdate(db *gorm.DB, store domain.Store)
	Delete(db *gorm.DB, userID string)
	SaveOrUpdateWarehouse(db *gorm.DB, warehouse domain.Warehouse)
	FindAllWarehouse(db *gorm.DB, storeID string) []domain.Warehouse
	FindWarehouseByID(db *gorm.DB, warehouseID string) (domain.Warehouse, error)
}
