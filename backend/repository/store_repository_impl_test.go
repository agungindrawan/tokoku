package repository

import (
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/util"
	"github.com/stretchr/testify/assert"
	"testing"
)

var storeRepository = NewStoreRepository()

var store = domain.Store{
	ID:     util.GenerateUUID(),
	UserID: "1",
	Name:   "Golang Store",
	Points: 100,
	Timestamp: domain.Timestamp{
		CreatedAt: util.GetUnixTimestamp(),
	},
}

func TestStoreRepositoryImpl_Delete(t *testing.T) {
	storeRepository.SaveOrUpdate(dbTest, store)
	storeRepository.Delete(dbTest, store.UserID)
}

func TestStoreRepositoryImpl_FindByUserID(t *testing.T) {
	storeRepository.SaveOrUpdate(dbTest, store)
	storeResponse, err := storeRepository.FindByUserID(dbTest, store.UserID)
	assert.Nil(t, err)
	assert.NotNil(t, storeResponse)

	storeRepository.Delete(dbTest, store.UserID)
}

func TestStoreRepositoryImpl_Save(t *testing.T) {
	storeRepository.SaveOrUpdate(dbTest, store)
	storeRepository.Delete(dbTest, store.UserID)
}
