package repository

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2/log"
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"gorm.io/gorm"
)

type StoreRepositoryImpl struct {
}

func NewStoreRepository() StoreRepositoryInterface {
	return &StoreRepositoryImpl{}
}

func (s StoreRepositoryImpl) FindByUserID(db *gorm.DB, userID string) (domain.Store, error) {

	var store domain.Store
	err := db.Take(&store, "user_id = ? ", userID).Error
	if err != nil {
		if err.Error() != "record not found" {
			log.Error(err)
			panic(err)
		}
		return store, errors.New(fmt.Sprint("store is not found"))
	}
	return store, nil
}

func (s StoreRepositoryImpl) SaveOrUpdate(db *gorm.DB, store domain.Store) {
	err := db.Save(&store).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (s StoreRepositoryImpl) Delete(db *gorm.DB, userID string) {
	err := db.Where("user_id = ?", userID).Delete(&domain.Store{}).Error
	if err != nil {
		log.Error()
		panic(err)
	}
}

func (s StoreRepositoryImpl) SaveOrUpdateWarehouse(db *gorm.DB, warehouse domain.Warehouse) {
	err := db.Save(&warehouse).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

func (s StoreRepositoryImpl) FindAllWarehouse(db *gorm.DB, storeID string) []domain.Warehouse {
	var warehouses []domain.Warehouse
	err := db.Where("warehouses.store_id = ?", storeID).Joins("join address on address.id = warehouses.address_id").Joins("Address").Find(&warehouses).Error
	if err != nil {
		log.Error(err)
		panic(err)
	}
	return warehouses
}

func (s StoreRepositoryImpl) FindWarehouseByID(db *gorm.DB, warehouseID string) (domain.Warehouse, error) {
	var warehouse domain.Warehouse
	err := db.Where("warehouses.id = ?", warehouseID).Joins("join address on address.id = warehouses.address_id").Joins("Address").Take(&warehouse).Error
	if err != nil {
		if err.Error() != "record not found" {
			log.Error(err)
			panic(err)
		}
		return warehouse, errors.New(fmt.Sprint("warehouse is not found"))
	}
	return warehouse, nil
}
