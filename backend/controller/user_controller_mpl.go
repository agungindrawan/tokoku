package controller

import (
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/service"
	"github.com/indrawanagung/shop_api_golang/util"
)

type UserControllerImpl struct {
	UserService service.UserServiceInterface
}

func NewUserController(userService service.UserServiceInterface) UserControllerInterface {
	return &UserControllerImpl{
		UserService: userService,
	}
}

func (c UserControllerImpl) Save(ctx *fiber.Ctx) error {
	userCreateRequest := new(web.UserCreateOrUpdateRequest)
	if err := ctx.BodyParser(userCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	userID := c.UserService.Save(*userCreateRequest)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"user_id": userID},
	}
	return ctx.Status(201).JSON(webResponse)
}

func (c UserControllerImpl) Update(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	userUpdateRequest := new(web.UserCreateOrUpdateRequest)
	if err := ctx.BodyParser(userUpdateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	c.UserService.Update(userID, *userUpdateRequest)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c UserControllerImpl) FindByID(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	userResponse := c.UserService.FindByID(userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   userResponse,
	}

	return ctx.Status(200).JSON(webResponse)
}

func (u UserControllerImpl) Delete(c *fiber.Ctx) error {
	userID := c.Params("userID")

	u.UserService.Delete(userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}

	return c.Status(200).JSON(webResponse)
}
