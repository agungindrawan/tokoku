package controller

import "github.com/gofiber/fiber/v2"

type ProductControllerInterface interface {
	FindAllProductCategory(ctx *fiber.Ctx) error
	SellerSaveProduct(ctx *fiber.Ctx) error
	SellerFindAllProduct(ctx *fiber.Ctx) error
	SellerFindByIDProduct(ctx *fiber.Ctx) error
	SellerUpdateProduct(ctx *fiber.Ctx) error
	SellerDeleteProduct(ctx *fiber.Ctx) error

	BuyerFindAllProduct(ctx *fiber.Ctx) error
	BuyerFindByIDProduct(ctx *fiber.Ctx) error
	BuyerFindCart(ctx *fiber.Ctx) error
	BuyerFindByIDCart(ctx *fiber.Ctx) error
	BuyerAddCart(ctx *fiber.Ctx) error
	BuyerUpdateCart(ctx *fiber.Ctx) error
	BuyerDeleteCart(ctx *fiber.Ctx) error

	BuyerSubmitOrder(ctx *fiber.Ctx) error

	BuyerFindAllOrder(ctx *fiber.Ctx) error
	BuyerOrderConfirmedPayment(ctx *fiber.Ctx) error
	FindAllPaymentType(ctx *fiber.Ctx) error
}
