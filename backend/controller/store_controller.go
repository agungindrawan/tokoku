package controller

import (
	"github.com/gofiber/fiber/v2"
)

type StoreControllerInterface interface {
	FindByUserID(ctx *fiber.Ctx) error
	SaveOrUpdate(ctx *fiber.Ctx) error
	Delete(ctx *fiber.Ctx) error
	SaveWarehouse(ctx *fiber.Ctx) error
	UpdateWarehouse(ctx *fiber.Ctx) error
	FindAllWarehouse(ctx *fiber.Ctx) error
	FindWarehouseByID(ctx *fiber.Ctx) error
}
