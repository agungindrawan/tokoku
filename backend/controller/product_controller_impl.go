package controller

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/service"
	"github.com/indrawanagung/shop_api_golang/util"
)

type ProductControllerImpl struct {
	ProductService service.ProductServiceInterface
}

func NewProductController(productService service.ProductServiceInterface) ProductControllerInterface {
	return &ProductControllerImpl{ProductService: productService}
}

func (c ProductControllerImpl) FindAllProductCategory(ctx *fiber.Ctx) error {
	productCategories := c.ProductService.FindAllProductCategory()

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   productCategories,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) SellerSaveProduct(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	productCreateRequest := new(web.ProductCreateOrUpdateRequest)
	if err := ctx.BodyParser(productCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	file, err := ctx.FormFile("image")
	if err != nil {
		panic(exception.NewBadRequestError("file image is not found"))
	}

	fileName := uuid.NewString() + ".jpg"
	// Save file to root directory:
	productCreateRequest.ImageURL = fileName
	err = ctx.SaveFile(file, fmt.Sprintf("./public/images/%s", fileName))
	if err != nil {
		log.Error(err)
		panic(err)
	}

	productID := c.ProductService.SaveProduct(*productCreateRequest, userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"id": productID},
	}
	return ctx.Status(201).JSON(webResponse)
}

func (c ProductControllerImpl) SellerFindAllProduct(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	products := c.ProductService.SellerFindAllProduct(userID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   products,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) SellerDeleteProduct(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	productID := ctx.Params("id")

	c.ProductService.SellerDeleteProduct(userID, productID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) SellerFindByIDProduct(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	productID := ctx.Params("id")

	productResponse := c.ProductService.SellerFindByIDProduct(userID, productID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   productResponse,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) SellerUpdateProduct(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	productID := ctx.Params("id")

	productCreateRequest := new(web.ProductCreateOrUpdateRequest)
	if err := ctx.BodyParser(productCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	fileName := ""
	file, err := ctx.FormFile("image")
	if err == nil {
		fileName = uuid.NewString() + ".jpg"
		err = ctx.SaveFile(file, fmt.Sprintf("./public/images/%s", fileName))
		if err != nil {
			log.Error(err)
			panic(err)
		}
	}

	// Save file to root directory:
	productCreateRequest.ImageURL = fileName

	c.ProductService.SellerUpdateProduct(userID, productID, *productCreateRequest)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"id": productID},
	}
	return ctx.Status(201).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerFindAllProduct(ctx *fiber.Ctx) error {
	//user := ctx.Locals("user").(*jwt.Token)
	//claims := user.Claims.(jwt.MapClaims)
	//userID := claims["id"].(string)

	products := c.ProductService.BuyerFindAllProduct()

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   products,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerFindByIDProduct(ctx *fiber.Ctx) error {
	//user := ctx.Locals("user").(*jwt.Token)
	//claims := user.Claims.(jwt.MapClaims)
	//userID := claims["id"].(string)
	productID := ctx.Params("id")

	productResponse := c.ProductService.BuyerFindByIDProduct(productID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   productResponse,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerFindCart(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	cartResponse := c.ProductService.BuyerFindCart(userID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   cartResponse,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerFindByIDCart(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	id := ctx.Params("id")

	cartProductResponse := c.ProductService.BuyerFindByIDCart(userID, id)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   cartProductResponse,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerAddCart(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	request := new(web.AddCartProduct)
	if err := ctx.BodyParser(request); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	id := c.ProductService.BuyerAddCartProduct(userID, *request)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data: map[string]string{
			"id": id,
		},
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerUpdateCart(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	id := ctx.Params("id")

	request := new(web.UpdateCartProduct)
	if err := ctx.BodyParser(request); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	c.ProductService.BuyerUpdateCartProduct(userID, *request, id)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerDeleteCart(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	id := ctx.Params("id")

	c.ProductService.BuyerDeleteCartProduct(userID, id)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerSubmitOrder(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	request := new(web.BuyerSubmitOrderRequest)
	if err := ctx.BodyParser(request); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}
	orderID := c.ProductService.BuyerSubmitOrder(userID, *request)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data: map[string]string{
			"id": orderID,
		},
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) FindAllPaymentType(ctx *fiber.Ctx) error {

	paymentTypeResponses := c.ProductService.FindAllPaymentType()
	fmt.Println(paymentTypeResponses)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   paymentTypeResponses,
	}
	fmt.Println("types", webResponse)
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerFindAllOrder(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	response := c.ProductService.BuyerFindAllOrder(userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   response,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c ProductControllerImpl) BuyerOrderConfirmedPayment(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	id := ctx.Params("id")

	c.ProductService.BuyerOrderConfirmedPayment(userID, id)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}
