package controller

import (
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/service"
	"github.com/indrawanagung/shop_api_golang/util"
)

type StoreControllerImpl struct {
	StoreService service.StoreServiceInterface
}

func NewStoreController(StoreService service.StoreServiceInterface) StoreControllerInterface {
	return &StoreControllerImpl{StoreService: StoreService}
}

func (c StoreControllerImpl) FindByUserID(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	storeResponse := c.StoreService.FindByUserID(userID)

	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   storeResponse,
	}
	return ctx.Status(200).JSON(webResponse)

}

func (c StoreControllerImpl) SaveOrUpdate(ctx *fiber.Ctx) error {
	storeCreateRequest := new(web.StoreCreateOrUpdateRequest)
	if err := ctx.BodyParser(storeCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	id := c.StoreService.SaveOrUpdate(*storeCreateRequest, userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"id": id},
	}
	return ctx.Status(201).JSON(webResponse)
}

func (c StoreControllerImpl) Delete(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	c.StoreService.Delete(userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   nil,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c StoreControllerImpl) SaveWarehouse(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	warehouseCreateRequest := new(web.WarehouseCreateOrUpdateRequest)
	if err := ctx.BodyParser(warehouseCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	warehouseID := c.StoreService.SaveWarehouse(*warehouseCreateRequest, userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"id": warehouseID},
	}
	return ctx.Status(201).JSON(webResponse)
}

func (c StoreControllerImpl) UpdateWarehouse(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)
	warehouseID := ctx.Params("warehouseID")

	warehouseCreateRequest := new(web.WarehouseCreateOrUpdateRequest)
	if err := ctx.BodyParser(warehouseCreateRequest); err != nil {
		panic(exception.NewBadRequestError(err.Error()))
	}

	response := c.StoreService.UpdateWarehouse(*warehouseCreateRequest, warehouseID, userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   map[string]string{"id": response},
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c StoreControllerImpl) FindAllWarehouse(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	warehouses := c.StoreService.FindAllWarehouse(userID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   warehouses,
	}
	return ctx.Status(200).JSON(webResponse)
}

func (c StoreControllerImpl) FindWarehouseByID(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["id"].(string)

	warehouseID := ctx.Params("warehouseID")

	warehouse := c.StoreService.FindWarehouseByID(userID, warehouseID)
	webResponse := web.WebResponse{
		Header: util.HeaderResponseSuccessfully(),
		Data:   warehouse,
	}
	return ctx.Status(200).JSON(webResponse)
}
