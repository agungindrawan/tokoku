package service

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2/log"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/repository"
	"github.com/indrawanagung/shop_api_golang/util"
	"gorm.io/gorm"
	"strconv"
)

type ProductServiceImpl struct {
	Database          *gorm.DB
	ProductRepository repository.ProductRepositoryInterface
	StoreRepository   repository.StoreRepositoryInterface
	UserRepository    repository.UserRepositoryInterface
	AddressRepository repository.AddressRepositoryInterface
	Validate          *validator.Validate
}

func NewProductService(database *gorm.DB, addressRepository repository.AddressRepositoryInterface, productRepository repository.ProductRepositoryInterface, storeRepository repository.StoreRepositoryInterface, userRepository repository.UserRepositoryInterface, validate *validator.Validate) ProductServiceInterface {
	return &ProductServiceImpl{
		Database:          database,
		ProductRepository: productRepository,
		StoreRepository:   storeRepository,
		UserRepository:    userRepository,
		AddressRepository: addressRepository,
		Validate:          validate,
	}
}

func (s ProductServiceImpl) FindAllProductCategory() []domain.ProductCategory {
	return s.ProductRepository.FindAllProductCategory(s.Database)
}

func (s ProductServiceImpl) SellerFindAllProduct(userID string) []web.SellerProductResponse {
	return s.ProductRepository.SellerFindAllProduct(s.Database, userID)
}

func (s ProductServiceImpl) SaveProduct(request web.ProductCreateOrUpdateRequest, userID string) string {
	err := s.Validate.Struct(request)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	_, err = s.ProductRepository.FindByIDProductCategory(s.Database, request.ProductCategoryID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	warehouse, err := s.StoreRepository.FindWarehouseByID(s.Database, request.WarehouseID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	if store.ID != warehouse.StoreID {
		log.Error(err)
		panic(exception.NewBadRequestError("cannot save product on this warehouse"))
	}

	product := domain.Product{
		ID:                util.GenerateUUID(),
		Name:              request.Name,
		ProductCategoryID: request.ProductCategoryID,
		StatusID:          "2",
		Price:             request.Price,
		WarehouseID:       request.WarehouseID,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
		},
	}

	s.ProductRepository.SellerSaveProduct(s.Database, product)
	s.ProductRepository.SaveImageProduct(s.Database, domain.ProductImage{
		ID:        util.GenerateUUID(),
		ProductID: product.ID,
		ImageURL:  request.ImageURL,
	})
	s.ProductRepository.SellerSaveProductStock(s.Database, domain.ProductStock{
		ID:          util.GenerateUUID(),
		ProductID:   product.ID,
		Total:       request.Stock,
		NotReserved: 0,
		Reserved:    0,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
		},
		Product: domain.Product{},
	})
	return product.ID
}

func (s ProductServiceImpl) SellerDeleteProduct(userID string, productID string) {
	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError("store is not found"))
	}

	product, err := s.ProductRepository.SellerFindByIDProduct(s.Database, userID, productID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	warehouse, err := s.StoreRepository.FindWarehouseByID(s.Database, product.WarehouseID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError("warehouse is not found"))
	}

	if store.ID != warehouse.StoreID {
		log.Error(err)
		panic(exception.NewBadRequestError("cannot delete this product"))
	}

	s.ProductRepository.SellerDeleteProduct(s.Database, productID)

}

func (s ProductServiceImpl) SellerFindByIDProduct(userID string, productID string) web.SellerProductResponse {
	product, err := s.ProductRepository.SellerFindByIDProduct(s.Database, userID, productID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	return product
}

func (s ProductServiceImpl) SellerUpdateProduct(userID string, productID string, request web.ProductCreateOrUpdateRequest) {
	findProduct, err := s.ProductRepository.SellerFindByIDProduct(s.Database, userID, productID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	if request.ImageURL == "" {
		request.ImageURL = findProduct.ImageUrl
	}

	err = s.Validate.Struct(request)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	_, err = s.ProductRepository.FindByIDProductCategory(s.Database, request.ProductCategoryID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	warehouse, err := s.StoreRepository.FindWarehouseByID(s.Database, request.WarehouseID)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	if store.ID != warehouse.StoreID {
		log.Error(err)
		panic(exception.NewBadRequestError("cannot save product on this warehouse"))
	}

	product := domain.Product{
		ID:                findProduct.ID,
		Name:              request.Name,
		ProductCategoryID: request.ProductCategoryID,
		StatusID:          "2",
		Price:             request.Price,
		WarehouseID:       request.WarehouseID,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
			UpdatedAt: util.GetUnixTimestamp(),
		},
	}

	s.ProductRepository.SellerSaveProduct(s.Database, product)
	//s.ProductRepository.SaveImageProduct(s.Database, domain.ProductImage{
	//	ID:        util.GenerateUUID(),
	//	ProductID: product.ID,
	//	ImageURL:  request.ImageURL,
	//})
	//s.ProductRepository.SellerSaveProductStock(s.Database, domain.ProductStock{
	//	ID:          util.GenerateUUID(),
	//	ProductID:   product.ID,
	//	Total:       request.Stock,
	//	NotReserved: 0,
	//	Reserved:    0,
	//	Timestamp: domain.Timestamp{
	//		CreatedAt: util.GetUnixTimestamp(),
	//	},
	//	Product: domain.Product{},
	//})

}

func (s ProductServiceImpl) BuyerFindAllProduct() []web.BuyerProductResponse {
	return s.ProductRepository.BuyerFindAllProduct(s.Database)
}

func (s ProductServiceImpl) BuyerFindByIDProduct(productID string) web.BuyerProductResponse {
	product, err := s.ProductRepository.BuyerFindByIDProduct(s.Database, productID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	return product
}

func (s ProductServiceImpl) BuyerFindCart(userID string) []web.CartResponse {
	return s.ProductRepository.BuyerFindCart(s.Database, userID)
}

func (s ProductServiceImpl) BuyerAddCartProduct(userID string, request web.AddCartProduct) string {
	err, _ := s.UserRepository.FindByID(s.Database, userID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	err = s.Validate.Struct(request)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	_, err = s.ProductRepository.BuyerFindByIDProduct(s.Database, request.ProductID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	cartProduct := domain.ShoppingCartItems{
		ID:        util.GenerateUUID(),
		ProductID: request.ProductID,
		UserID:    userID,
		Qty:       request.Qty,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GenerateUUID(),
		},
	}
	s.ProductRepository.BuyerAddCartProduct(s.Database, cartProduct)
	return cartProduct.ID

}

func (s ProductServiceImpl) BuyerFindByIDCart(userID string, cartID string) web.CartResponse {
	cart, err := s.ProductRepository.BuyerFindByIDCart(s.Database, userID, cartID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	return cart
}

func (s ProductServiceImpl) BuyerUpdateCartProduct(userID string, request web.UpdateCartProduct, cartID string) {
	err := s.Validate.Struct(request)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	findCart, err := s.ProductRepository.BuyerFindByIDCart(s.Database, userID, cartID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	cart := domain.ShoppingCartItems{
		ID:        findCart.ID,
		ProductID: findCart.ProductID,
		UserID:    findCart.UserID,
		Qty:       request.Qty,
	}
	fmt.Println("tewst", cart)
	s.ProductRepository.BuyerUpdateCartProduct(s.Database, cart)
}

func (s ProductServiceImpl) BuyerDeleteCartProduct(userID string, cartID string) {

	_, err := s.ProductRepository.BuyerFindByIDCart(s.Database, userID, cartID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	s.ProductRepository.BuyerDeleteCartProduct(s.Database, userID, cartID)
}

func (s ProductServiceImpl) BuyerSubmitOrder(userID string, request web.BuyerSubmitOrderRequest) string {
	tx := s.Database.Begin()
	defer tx.Rollback()

	err := s.Validate.Struct(request)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	err, _ = s.AddressRepository.FindByID(tx, request.AddressID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	err, _ = s.UserRepository.FindByID(tx, userID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	var totalPrice int64 = 0

	cartProducts := s.ProductRepository.BuyerFindCart(tx, userID)

	for _, p := range cartProducts {
		totalPrice += p.Price * int64(p.Qty)
		s.ProductRepository.BuyerDeleteCartProduct(tx, userID, p.ID)
	}

	atoi, err := strconv.Atoi(util.GetUnixTimestamp())
	if err != nil {
		log.Error(err)
		panic(err)
	}
	atoi += 100000 * 3 //3 days
	expiryDate := strconv.Itoa(atoi)
	paymentMethod := domain.PaymentMethod{
		ID:            util.GenerateUUID(),
		PaymentTypeID: request.PaymentTypeID,
		AccountNumber: util.GetUnixTimestamp(),
		ExpiryDate:    expiryDate,
		StatusID:      "4",
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
		}}
	s.ProductRepository.SavePaymentMethod(tx, paymentMethod)

	order := domain.ShopOrder{
		ID:              util.GenerateUUID(),
		UserID:          userID,
		AddressID:       request.AddressID,
		TotalPrice:      totalPrice,
		StatusID:        "5",
		PaymentMethodID: paymentMethod.ID,
		OrderedAt:       util.GetUnixTimestamp(),
	}

	s.ProductRepository.SubmitOrder(tx, order)

	for _, p := range cartProducts {
		s.ProductRepository.SubmitOrderItem(tx, domain.ShopOrderItem{
			ID:          util.GenerateUUID(),
			ProductID:   p.ProductID,
			ShopOrderID: order.ID,
			Qty:         p.Qty,
			Price:       p.Price,
		})
	}

	if err == nil {
		tx.Commit()
	}

	return order.ID
}

func (s ProductServiceImpl) FindAllPaymentType() []domain.PaymentType {
	paymentTypes := s.ProductRepository.FindAllPaymentType(s.Database)
	return paymentTypes
}

func (s ProductServiceImpl) BuyerFindAllOrder(userID string) []web.BuyerOrderResponse {
	return s.ProductRepository.BuyerFindAllOrder(s.Database, userID)
}

func (s ProductServiceImpl) BuyerOrderConfirmedPayment(userID string, orderID string) {
	_, err := s.ProductRepository.BuyerFindOrderByID(s.Database, userID, orderID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	order := domain.ShopOrder{
		ID:       orderID,
		UserID:   userID,
		StatusID: "6",
	}
	s.ProductRepository.BuyerOrderConfirmedPayment(s.Database, order)
}
