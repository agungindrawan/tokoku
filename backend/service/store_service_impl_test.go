package service

import (
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/repository"
	"github.com/stretchr/testify/assert"
	"testing"
)

var storeRepository = repository.NewStoreRepository()
var storeService = NewStoreService(database, validate, storeRepository)

var storeRequest = web.StoreCreateOrUpdateRequest{Name: "GolangStore"}
var userID = "1"

func TestStoreServiceImpl_Delete(t *testing.T) {
	storeService.SaveOrUpdate(storeRequest, userID)
	storeService.Delete(userID)
}

func TestStoreServiceImpl_FindByUserID(t *testing.T) {
	storeService.SaveOrUpdate(storeRequest, userID)
	store := storeService.FindByUserID(userID)
	assert.Equal(t, storeRequest.Name, store.Name)
	storeService.Delete(userID)
}
