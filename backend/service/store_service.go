package service

import (
	"github.com/indrawanagung/shop_api_golang/model/web"
)

type StoreServiceInterface interface {
	FindByUserID(userID string) web.StoreResponse
	SaveOrUpdate(storeRequest web.StoreCreateOrUpdateRequest, userID string) string
	Delete(userID string)
	SaveWarehouse(warehouseRequest web.WarehouseCreateOrUpdateRequest, userID string) string
	UpdateWarehouse(warehouseRequest web.WarehouseCreateOrUpdateRequest, warehouseID string, userID string) string
	FindAllWarehouse(userID string) []web.WarehouseResponse
	FindWarehouseByID(userID string, warehouseID string) web.WarehouseResponse
}
