package service

import (
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/model/web"
)

type ProductServiceInterface interface {
	FindAllProductCategory() []domain.ProductCategory
	SaveProduct(request web.ProductCreateOrUpdateRequest, userID string) string
	SellerFindAllProduct(userID string) []web.SellerProductResponse
	SellerFindByIDProduct(userID string, productID string) web.SellerProductResponse
	SellerUpdateProduct(userID string, productID string, request web.ProductCreateOrUpdateRequest)
	SellerDeleteProduct(userID string, productID string)

	BuyerFindAllProduct() []web.BuyerProductResponse
	BuyerFindByIDProduct(productID string) web.BuyerProductResponse

	BuyerFindCart(userID string) []web.CartResponse
	BuyerFindByIDCart(userID string, cartID string) web.CartResponse

	BuyerAddCartProduct(userID string, request web.AddCartProduct) string
	BuyerUpdateCartProduct(userID string, request web.UpdateCartProduct, cartID string)
	BuyerDeleteCartProduct(userID string, cartID string)

	BuyerSubmitOrder(userID string, request web.BuyerSubmitOrderRequest) string

	BuyerFindAllOrder(userID string) []web.BuyerOrderResponse
	BuyerOrderConfirmedPayment(userID string, orderID string)
	FindAllPaymentType() []domain.PaymentType
}
