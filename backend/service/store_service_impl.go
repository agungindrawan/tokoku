package service

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2/log"
	"github.com/indrawanagung/shop_api_golang/exception"
	"github.com/indrawanagung/shop_api_golang/model/domain"
	"github.com/indrawanagung/shop_api_golang/model/web"
	"github.com/indrawanagung/shop_api_golang/repository"
	"github.com/indrawanagung/shop_api_golang/util"
	"gorm.io/gorm"
)

type StoreServiceImpl struct {
	Database          *gorm.DB
	Validate          *validator.Validate
	StoreRepository   repository.StoreRepositoryInterface
	AddressRepository repository.AddressRepositoryInterface
}

func NewStoreService(Database *gorm.DB,
	Validate *validator.Validate,
	StoreRepository repository.StoreRepositoryInterface,
	AddressRepository repository.AddressRepositoryInterface) StoreServiceInterface {
	return &StoreServiceImpl{
		Database:          Database,
		Validate:          Validate,
		StoreRepository:   StoreRepository,
		AddressRepository: AddressRepository,
	}
}

func (s StoreServiceImpl) FindByUserID(userID string) web.StoreResponse {
	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	return web.ToStoreResponse(store)
}

func (s StoreServiceImpl) SaveOrUpdate(storeRequest web.StoreCreateOrUpdateRequest, userID string) string {
	err := s.Validate.Struct(storeRequest)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	store := domain.Store{
		ID:     util.GenerateUUID(),
		UserID: userID,
		Name:   storeRequest.Name,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
		},
	}
	s.StoreRepository.SaveOrUpdate(s.Database, store)
	return store.ID
}

func (s StoreServiceImpl) Delete(userID string) {
	s.StoreRepository.Delete(s.Database, userID)
}

func (s StoreServiceImpl) SaveWarehouse(warehouseRequest web.WarehouseCreateOrUpdateRequest, userID string) string {
	err := s.Validate.Struct(warehouseRequest)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	//validate address ID
	err, _ = s.AddressRepository.FindByID(s.Database, warehouseRequest.AddressID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	warehouse := domain.Warehouse{
		ID:        util.GenerateUUID(),
		StoreID:   store.ID,
		AddressID: warehouseRequest.AddressID,
		StatusID:  "1",
		Name:      warehouseRequest.Name,
		Timestamp: domain.Timestamp{
			CreatedAt: util.GetUnixTimestamp(),
		},
	}
	s.StoreRepository.SaveOrUpdateWarehouse(s.Database, warehouse)
	return warehouse.ID
}

func (s StoreServiceImpl) UpdateWarehouse(warehouseRequest web.WarehouseCreateOrUpdateRequest, warehouseID string, userID string) string {
	err := s.Validate.Struct(warehouseRequest)
	errTrans := util.TranslateErroValidation(s.Validate, err)
	if err != nil {
		log.Error(err)
		panic(exception.NewBadRequestError(errTrans.Error()))
	}

	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	getWarehouse, err := s.StoreRepository.FindWarehouseByID(s.Database, warehouseID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	if store.ID != getWarehouse.StoreID {
		panic(exception.NewBadRequestError("cannot update this warehouse"))
	}

	//validate address ID
	err, _ = s.AddressRepository.FindByID(s.Database, warehouseRequest.AddressID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}

	warehouse := domain.Warehouse{
		ID:        warehouseID,
		StoreID:   store.ID,
		AddressID: warehouseRequest.AddressID,
		StatusID:  "1",
		Name:      warehouseRequest.Name,
		Timestamp: domain.Timestamp{
			CreatedAt: getWarehouse.CreatedAt,
			UpdatedAt: util.GetUnixTimestamp(),
		},
	}
	s.StoreRepository.SaveOrUpdateWarehouse(s.Database, warehouse)
	return warehouse.ID
}

func (s StoreServiceImpl) FindAllWarehouse(userID string) []web.WarehouseResponse {
	store, err := s.StoreRepository.FindByUserID(s.Database, userID)
	if err != nil {
		log.Info(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	warehouses := s.StoreRepository.FindAllWarehouse(s.Database, store.ID)
	//if len(warehouses) < 1 {
	//	fmt.Println(12313)
	//	return nil
	//}
	return web.ToWarehouseResponses(warehouses)
}

func (s StoreServiceImpl) FindWarehouseByID(userID string, warehouseID string) web.WarehouseResponse {

	warehouse, err := s.StoreRepository.FindWarehouseByID(s.Database, warehouseID)
	if err != nil {
		log.Error(err)
		panic(exception.NewNotFoundError(err.Error()))
	}
	return web.ToWarehouseResponse(warehouse)
}
