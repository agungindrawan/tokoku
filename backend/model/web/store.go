package web

import "github.com/indrawanagung/shop_api_golang/model/domain"

type StoreCreateOrUpdateRequest struct {
	Name string `validate:"required,min=5,max=50" json:"name"`
}

type StoreResponse struct {
	ID        string `json:"id"`
	UserID    string `json:"user_id"`
	Name      string `json:"name"`
	Points    int64  `json:"points"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func ToStoreResponse(store domain.Store) StoreResponse {
	return StoreResponse{
		ID:        store.ID,
		UserID:    store.UserID,
		Name:      store.Name,
		Points:    store.Points,
		CreatedAt: store.Timestamp.CreatedAt,
		UpdatedAt: store.Timestamp.UpdatedAt,
	}
}
