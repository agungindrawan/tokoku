package web

import "github.com/indrawanagung/shop_api_golang/model/domain"

type ProductCreateOrUpdateRequest struct {
	Name              string `validate:"required,min=5,max=50" json:"name" form:"name"`
	ProductCategoryID string `validate:"required,min=1,max=50" json:"product_category_id" form:"product_category_id"`
	WarehouseID       string `validate:"required,min=1,max=50" json:"warehouse_id" form:"warehouse_id"`
	ImageURL          string `validate:"required"`
	Price             int64  `validate:"required,gt=0" json:"price" form:"price"`
	Stock             int    `validate:"required" json:"stock" form:"stock"`
}

type BuyerSubmitOrderRequest struct {
	AddressID     string `validate:"required" json:"address_id"`
	PaymentTypeID string `validate:"required" json:"payment_type_id"`
}

type BuyerOrderResponse struct {
	ID              string                   `json:"id"`
	UserID          string                   `json:"user_id"`
	AddressID       string                   `json:"address_id"`
	AddressName     string                   `json:"address_name"`
	TotalPrice      int64                    `json:"total_price"`
	OrderedAt       string                   `json:"ordered_at"`
	PaymentMethodID string                   `json:"payment_method_id"`
	StatusID        string                   `json:"status_id"`
	StatusName      string                   `json:"status_name"`
	StatusColor     string                   `json:"status_color"`
	Items           []BuyerOrderItemResponse `json:"items"`
}

type BuyerOrderScan struct {
	ID              string `json:"id"`
	UserID          string `json:"user_id"`
	AddressID       string `json:"address_id"`
	AddressName     string `json:"address_name"`
	TotalPrice      int64  `json:"total_price"`
	OrderedAt       string `json:"ordered_at"`
	PaymentMethodID string `json:"payment_method_id"`
	StatusID        string `json:"status_id"`
	StatusName      string `json:"status_name"`
	StatusColor     string `json:"status_color"`
}

type SellerProductResponse struct {
	ID                string `json:"id"`
	Name              string `json:"name"`
	ProductCategoryID string `json:"product_category_id"`
	CategoryName      string `json:"product_category_name"`
	WarehouseID       string `json:"warehouse_id"`
	Price             int64  `json:"price"`
	StatusName        string `json:"status_name"`
	Stock             string `json:"stock"`
	ImageUrl          string `json:"image_url"`
}

type BuyerProductResponse struct {
	ID                string `json:"id"`
	Name              string `json:"name"`
	ProductCategoryID string `json:"product_category_id"`
	CategoryName      string `json:"product_category_name"`
	WarehouseID       string `json:"warehouse_id"`
	Price             int64  `json:"price"`
	StatusName        string `json:"status_name"`
	Stock             string `json:"stock"`
	ImageUrl          string `json:"image_url"`
}

type BuyerOrderItemResponse struct {
	ID          string `json:"id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Qty         int    `json:"qty"`
	ImageUrl    string `json:"image_url"`
	Price       int64  `json:"price"`
}

type CartResponse struct {
	ID          string `json:"id"`
	ProductID   string `json:"product_id"`
	UserID      string `json:"user_id"`
	Qty         int    `json:"qty"`
	ProductName string `json:"product_name"`
	Price       int64  `json:"price"`
	ImageUrl    string `json:"image_url"`
}

type AddCartProduct struct {
	ProductID string `json:"product_id"`
	Qty       int    `json:"qty"`
}

type UpdateCartProduct struct {
	Qty int `json:"qty"`
}

func ToProductResponse(product domain.Product) SellerProductResponse {
	return SellerProductResponse{
		ID:                product.ID,
		Name:              product.Name,
		ProductCategoryID: product.ProductCategoryID,
		//ProductCategoryName: product.ProductCategory.CategoryName,
		WarehouseID: product.WarehouseID,
		Price:       product.Price,
		StatusName:  product.Status.Name,
		//ImageUrl:            product.ProductImage.ImageURL,
	}
}

func ToProductResponses(products []domain.Product) []SellerProductResponse {
	var productResponses []SellerProductResponse

	for _, product := range products {
		productResponses = append(productResponses, ToProductResponse(product))
	}
	return productResponses
}
