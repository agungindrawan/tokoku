package web

import "github.com/indrawanagung/shop_api_golang/model/domain"

type WarehouseCreateOrUpdateRequest struct {
	AddressID string `validate:"required,min=5,max=50" json:"address_id"`
	Name      string `validate:"required,min=5,max=50" json:"name"`
}

type WarehouseResponse struct {
	ID          string `json:"id"`
	StoreID     string `json:"store_id"`
	AddressID   string `json:"address_id"`
	StatusID    string `json:"status_id"`
	Name        string `json:"name"`
	AddressName string `json:"address_name"`
}

func ToWarehouseResponse(warehouse domain.Warehouse) WarehouseResponse {
	return WarehouseResponse{
		ID:          warehouse.ID,
		StoreID:     warehouse.StoreID,
		AddressID:   warehouse.AddressID,
		StatusID:    warehouse.StatusID,
		Name:        warehouse.Name,
		AddressName: warehouse.Address.Name,
	}
}

func ToWarehouseResponses(warehouses []domain.Warehouse) []WarehouseResponse {
	var warehouseResponses []WarehouseResponse
	for _, warehouse := range warehouses {
		warehouseResponses = append(warehouseResponses, ToWarehouseResponse(warehouse))
	}
	return warehouseResponses
}
