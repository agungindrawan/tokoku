package domain

type PaymentMethod struct {
	ID            string `gorm:"primaryKey"`
	PaymentTypeID string
	AccountNumber string
	ExpiryDate    string
	StatusID      string
	Timestamp
}

func (p *PaymentMethod) TableName() string {
	return "payment_methods"
}
