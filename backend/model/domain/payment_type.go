package domain

import "gorm.io/gorm"

type PaymentType struct {
	ID        string         `gorm:"primaryKey" json:"id"`
	Name      string         `json:"name"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}

func (p *PaymentType) TableName() string {
	return "payment_types"
}
