package domain

type ShoppingCartItems struct {
	ID        string `gorm:"primaryKey"`
	ProductID string
	UserID    string
	Qty       int
	Timestamp
	Product Product `gorm:"foreignKey:product_id;references:id"`
	User    User    `gorm:"foreignKey:user_id;references:id"`
}

func (i *ShoppingCartItems) TableName() string {
	return "shopping_cart_items"
}
