package domain

type ShopOrderItem struct {
	ID          string `gorm:"primaryKey"`
	ProductID   string
	ShopOrderID string
	Qty         int
	Price       int64
}

func (s *ShopOrderItem) TableName() string {
	return "shop_order_items"
}
