/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
          domains: ['example.com', 'www.static-src.com','orfarm-next-js.vercel.app', 'img.freepik.com', 'www.freepik.com' , 'localhost'],
        },
};

export default nextConfig;
