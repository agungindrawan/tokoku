"use client"
import { Container } from "postcss";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { usePathname } from 'next/navigation'
import { CirclesFour, MapPin, User } from "@phosphor-icons/react";

export default function SidebarAcccount() {
   const router = useRouter();
   const activeClass = "flex items-center py-3 text-black rounded-lg dark:text-white bg-gray-100 dark:hover:bg-gray-700 group"
   const disableClass = "flex items-center py-3 text-black rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group"
   return (
      <>
         <aside id="default-sidebar" className="fixed top-0 w-48 mt-8 border-r-2 border-black border-opacity-10 h-screen transition-transform -translate-x-full sm:translate-x-0" aria-label="Sidebar">
            <div className="h-full overflow-y-auto">
               <ul className="space-y-2 font-medium">
                  <li>
                     <p className="text-2xl font-semibold">Tokoku</p>
                  </li>

                  <li>
                     <a href="/home" className={disableClass}>
                        <CirclesFour size={25} />
                        <span className="flex-1 ms-3 whitespace-nowrap">Home</span>
                     </a>
                  </li>
                  <li>
                     <a href="/account/profile" className={usePathname() == "/account/mystore" ? activeClass : disableClass}>
                        <User size={25} />
                        <span className="flex-1 ms-3 whitespace-nowrap">Profile</span>
                     </a>
                  </li>
                  <li>
                     <a href="/account/address" className={usePathname() == "/account/address" ? activeClass : disableClass}>
                        <MapPin size={25} />
                        <span className="flex-1 ms-3 whitespace-nowrap">Address</span>
                        {/* <span className="inline-flex items-center justify-center w-3 h-3 p-3 ms-3 text-sm font-medium text-blue-800 bg-blue-100 rounded-full dark:bg-blue-900 dark:text-blue-300">3</span> */}
                     </a>
                  </li>
                  <li>
                  </li>
               </ul>
            </div>
         </aside>
      </>
   );
}
