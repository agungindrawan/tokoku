type AlertProps = {
    message: string
}
export default function Alert(props: AlertProps) {
    return (
        <>
            <div className="p-4 mb-4 text-sm text-red-900 rounded-lg bg-red-100 dark:bg-gray-800 dark:text-red-400" role="alert">
                <span className="font-medium">Danger alert! </span> {props.message}
            </div>
        </>
    )
}