"use client";
import { Note, ShoppingCart, SignOut, Storefront, User } from "@phosphor-icons/react";
import Cookies from "js-cookie";
import Link from "next/link";
import { useState } from "react";

export default function Navbar() {
  const token = Cookies.get("token");

  const [isActiveDropdown, setIsActiveDropdown] = useState(false);

  const handleDropdownButton = () => {
    setIsActiveDropdown(!isActiveDropdown);
  };


  const classActiveDropdown =
    "absolute right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none";
  const classDisableDropdown =
    "absolute hidden right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none";
  // const handleClickSignIn = () => {
  //   router.push("/auth/signin")
  // }
  // const handleClickAccount = () => {
  //   router.push("/account/mystore")
  // }
  return (
    <>
      <div className="border-gray-200 sticky top-0 z-50 pt-6 pb-5 rounded-br-lg rounded-bl-lg bg-gray-200 dark:bg-gray-900">
        <div className="flex flex-wrap items-center justify-between mx-auto w-[1111px]">
          <Link href="/home" className="self-center px-2 py-2 text-2xl text-white rounded-xl rounded-tl-none bg-black font-semibold hover:bg-gray-500">
            CheckoutStore
          </Link>

          <form className="flex items-center w-[50%] mx-auto">
            <div className="relative w-full">
              <input
                type="text"
                id="voice-search"
                className="bg-gray-50 border border-gray-300 text-sm rounded-lg  w-full p-2.5  dark:placeholder-gray-400 dark:text-white"
                placeholder="Search Product"
                required
              />
            </div>
            <button
              type="submit"
              className="inline-flex btn items-center px-3 py-1 ms-2 text-sm font-medium text-white hover:bg-gray-500 bg-black ... rounded-lg focus:outline-none"
            >
              Search
            </button>
          </form>
            <div className="flex gap-2" id="navbar-default">
              <a
                href="/home/cart"
                className="py-2 px-1 text-black font-light text-sm  rounded-lg"
                aria-current="page"
              >
                <ShoppingCart size={25} className="font-light" />
              </a>
              <a href="#" className="py-2 px-1 text-black">
              <Note className="" size={25} />
              </a>
              <div className="relative inline-block text-left">
                <button
                  onClick={handleDropdownButton}
                  type="button"
                  className="text-black px-1 py-2"
                  id="menu-button"
                  aria-expanded="true"
                  aria-haspopup="true"
                >
                  <User size={25} />
                </button>

                <div
                  id="listdropdown"
                  className={
                    isActiveDropdown
                      ? classActiveDropdown
                      : classDisableDropdown
                  }
                  role="menu"
                  aria-orientation="vertical"
                  aria-labelledby="menu-button"
                >
                  <a
                    href="/account/profile"
                    className="text-gray-700 w-full flex gap-2 font-medium hover:bg-gray-100  px-4 py-2 text-sm"
                    role="menuitem"
                    id="menu-item-0"
                  >
                    {" "}
                    <User size={20} />
                    Profile
                  </a>
                  <a
                    href="/store/mystore"
                    className="text-gray-700 w-full flex gap-2 font-medium hover:bg-gray-100  px-4 py-2 text-sm"
                    role="menuitem"
                    id="menu-item-1"
                  >
                    <Storefront size={20} />
                    My Store
                  </a>
                  <a
                    href="#"
                    className="text-gray-700 w-full flex gap-2 font-medium hover:bg-gray-100  px-4 py-2 text-sm"
                    role="menuitem"
                    id="menu-item-1"
                  >
                    {" "}
                    <SignOut size={20} />
                    Sign Out
                  </a>
                </div>
              </div>
            </div>
        </div>
      </div>
    </>
  );
}
