import {IUserGetProfileRequest, IUserGetProfileResponse, IUserUpdateRequest, IUserUpdateResponse } from "@/interface/api_user"

export const getUserProfile = async(request : IUserGetProfileRequest) : Promise<IUserGetProfileResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users`,  { 
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${request.token}`
            },
        })
        const responseJson = await response.json()
        const userGetProfileResponse : IUserGetProfileResponse = {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return userGetProfileResponse
}

export const putUserUpdate = async(request : IUserUpdateRequest) : Promise<IUserUpdateResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users`,  { 
          cache: 'no-store',
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${request.token}`
          },
          body: JSON.stringify({
            full_name : request.full_name,
            email_address : request.email_address,
            phone_number : request.phone_number,
            password : request.password
          })
      })
      const responseJson = await response.json()
      const userUpdateResponse : IUserUpdateResponse = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : null
      }
      return userUpdateResponse
}