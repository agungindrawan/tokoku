import { ISignInRequest, ISignInResponse } from "@/interface/api_auth"

export const postSignIn = async(request : ISignInRequest) : Promise<ISignInResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/auth/login`,  { 
            cache: 'no-store',
            method: "POST", 
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(request), 
        })
        const responseJson = await response.json()
        const signInResponse : ISignInResponse= {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return signInResponse
}