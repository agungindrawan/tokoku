import { IGetListAddressResponse } from "@/interface/api_address"
import { IAddCartProductResponse, ICreateProductResponse, IDeleteProductResponse, IGetCartProductBuyerResponse, IGetCartProductByIdBuyerResponse, IGetListOrderResponse, IGetListProductBuyerResponse, IGetListProductCategoryResponse, IGetListProductSellerResponse, IGetProductBuyerByIDResponse, IGetProductSellerByIDResponse, INullResponse, ISubmitOrderRequest, ISubmitOrderResponse, IUpdateCartProductResponse } from "@/interface/api_product"
import { StringOrUndefined } from "@/interface/types"
import axios from "axios"

export const getListProduct = async(token : StringOrUndefined) : Promise<IGetListProductSellerResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products`,  { 
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${token}`
            },
        })
        const responseJson = await response.json()
        return {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
}

export const getListProductBuyer = async () : Promise<IGetListProductBuyerResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/products`,  { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            // "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}

export const getProductBuyerByID = async(id : string) : Promise<IGetProductBuyerByIDResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/products/${id}`,  { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
      })
      const responseJson = await response.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}



export const getProductByID = async(token : StringOrUndefined, id : string) : Promise<IGetProductSellerByIDResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products/${id}`,  { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}


export const getListProductCategory = async(token : StringOrUndefined) : Promise<IGetListProductCategoryResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products/categories`,  { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}

export const postSaveProduct = async(formData : FormData, token : StringOrUndefined) : Promise<ICreateProductResponse> => {
  const response = await axios({
        method: 'post',
        url: `${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products`,
        validateStatus: () => true,
        headers: {
          Authorization : `Bearer ${token}`
        },
        data : formData
  });    
      
  return {
         header : {
          message : response.data.header.message,
          error : response.data.header.error,
         },
         data : response.data.data
      }
}

export const putEditProduct = async(formData : FormData, id : string, token : StringOrUndefined) : Promise<ICreateProductResponse> => {
  const response = await axios({
        method: 'put',
        url: `${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products/${id}`,
        validateStatus: () => true,
        headers: {
          Authorization : `Bearer ${token}`
        },
        data : formData
  });    
      
  return {
         header : {
          message : response.data.header.message,
          error : response.data.header.error,
         },
         data : response.data.data
      }
}

export const deleteProduct = async(token : StringOrUndefined, id : string) : Promise<IDeleteProductResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/products/${id}`, { 
          cache: 'no-store',
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          }
      })
      const responseJson = await response.json()
      const responseAPI : IDeleteProductResponse = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : null
      }
      return responseAPI
}

export const getCartProduct = async(token : StringOrUndefined) : Promise<IGetCartProductBuyerResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/carts`, { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          }
      })
      const responseJson : IGetCartProductBuyerResponse = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}

export const getCartProductById = async(token : StringOrUndefined, id : string) : Promise<IGetCartProductByIdBuyerResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/carts/${id}`, { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          }
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}

export const postCartProduct = async(token : StringOrUndefined, product_id : string, qty : number) : Promise<IAddCartProductResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/carts`, { 
          cache: 'no-store',
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
          body : JSON.stringify({
            product_id : product_id,
            qty : qty
          })
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}
export const updateCartProduct = async(token : StringOrUndefined, id : string, qty : number) : Promise<IUpdateCartProductResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/carts/${id}`, { 
          cache: 'no-store',
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
          body : JSON.stringify({
            qty : Number(qty)
          })
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}

export const deleteCartProduct = async(token : StringOrUndefined, id : string) : Promise<IDeleteProductResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/carts/${id}`, { 
          cache: 'no-store',
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}

export const postSubmitOrder = async(token : StringOrUndefined, request : ISubmitOrderRequest) : Promise<ISubmitOrderResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/orders/submit`, { 
          cache: 'no-store',
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
          body : JSON.stringify(request)
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}


export const getListOrder = async(token : StringOrUndefined) : Promise<IGetListOrderResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/orders`, { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}

export const confirmedPayment = async(token : StringOrUndefined, orderID : string) : Promise<INullResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/orders/confirmed_payment/${orderID}`, { 
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await response.json()
      const responseAPI = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}