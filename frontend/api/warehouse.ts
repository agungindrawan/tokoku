import {ICreateWarehouseRequest, ICreateWarehouseResponse, IGetWarehouseByIDResponse, IGetWarehouseResponse, IUpdateWarehouseRequest, IUpdateWarehouseResponse} from "@/interface/api_warehouse";

export const getListWarehouse = async(token : string | undefined) : Promise<IGetWarehouseResponse> => {
    const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/warehouses`,  {
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${token}`
            },
        })
        const responseJson = await responseAPI.json()
        return {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
}

export const getWarehouseByID = async(token : string | undefined, warehouseID : string) : Promise<IGetWarehouseByIDResponse> => {
  const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/warehouses/${warehouseID}`,  {
          cache: 'no-store',
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          },
      })
      const responseJson = await responseAPI.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}


export const postSaveWarehouse = async(request : ICreateWarehouseRequest) : Promise<ICreateWarehouseResponse> => {
  const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/warehouses`,  {
          cache: 'no-store',
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${request.token}`
          },
          body : JSON.stringify({
            name : request.name,
            address_id : request.address_id
          })
      })
      const responseJson = await responseAPI.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}

export const putUpdateWarehouse = async(request : IUpdateWarehouseRequest) : Promise<IUpdateWarehouseResponse> => {
  const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/warehouses/${request.id}`,  {
          cache: 'no-store',
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${request.token}`
          },
          body : JSON.stringify({
            name : request.name,
            address_id : request.address_id
          })
      })
      const responseJson = await responseAPI.json()
      return {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
}
