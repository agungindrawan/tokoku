import { ICreateAddressRequest, ICreateAddressResponse, IDeleteAddressResponse, IGetListAddressRequest, IGetListAddressResponse } from "@/interface/api_address"
import { StringOrUndefined } from "@/interface/types"
import {ICreateStoreRequest, ICreateStoreResponse, IGetStoreResponse} from "@/interface/api_store";

export const getStore = async(token : string | undefined) : Promise<IGetStoreResponse> => {
    const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/store`,  {
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${token}`
            },
        })
        const responseJson = await responseAPI.json()
        const response : IGetStoreResponse = {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return response
}


export const postSaveStore = async(request : ICreateStoreRequest) : Promise<ICreateStoreResponse> => {
  const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/seller/store`,  {
          cache: 'no-store',
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${request.token}`
          },
          body : JSON.stringify({
            name : request.name,
          })
      })
      const responseJson = await responseAPI.json()
      const response : ICreateStoreResponse = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return response
}
