import { IGetListCityResponse } from "@/interface/api_city"
import { StringOrUndefined } from "@/interface/types"
import { list } from "postcss"

export const getListCity = async(token : StringOrUndefined) : Promise<IGetListCityResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/cities`,  { 
            cache: 'no-store',
            method: "GET", 
            headers: {
              "Content-Type": "application/json",
            },
        })
        const responseJson = await response.json()
        const listCityResponse : IGetListCityResponse = {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return listCityResponse
}