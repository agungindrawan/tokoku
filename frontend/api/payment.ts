import { IGetListPaymentTypeResponse } from "@/interface/api_product"
import { StringOrUndefined } from "@/interface/types"

export const getListPaymentType = async(token : StringOrUndefined) : Promise<IGetListPaymentTypeResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/payments/type`, { 
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${token}`
            }
        })
        const responseJson = await response.json()
        const responseAPI = {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return responseAPI
  }