import { ICreateAddressRequest, ICreateAddressResponse, IDeleteAddressResponse, IGetListAddressRequest, IGetListAddressResponse } from "@/interface/api_address"
import { StringOrUndefined } from "@/interface/types"

export const getListAddress = async(request : IGetListAddressRequest) : Promise<IGetListAddressResponse> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/address`,  { 
            cache: 'no-store',
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              "authorization" : `Bearer ${request.token}`
            },
        })
        const responseJson = await response.json()
        const userGetProfileResponse : IGetListAddressResponse = {
           header : {
            message : responseJson.header.message,
            error : responseJson.header.error,
           },
           data : responseJson.data
        }
        return userGetProfileResponse
}


export const postSaveAddress = async(request : ICreateAddressRequest) : Promise<ICreateAddressResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/address`,  { 
          cache: 'no-store',
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${request.token}`
          },
          body : JSON.stringify({
            name : request.name,
            city_id :request.city_id,
            postal_code : request.postal_code,
          })
      })
      const responseJson = await response.json()
      const responseAPI : ICreateAddressResponse = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}


export const deleteAddress = async(token : StringOrUndefined, id : string) : Promise<IDeleteAddressResponse> => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/address/${id}`, { 
          cache: 'no-store',
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "authorization" : `Bearer ${token}`
          }
      })
      const responseJson = await response.json()
      const responseAPI : IDeleteAddressResponse = {
         header : {
          message : responseJson.header.message,
          error : responseJson.header.error,
         },
         data : responseJson.data
      }
      return responseAPI
}