export const getDateUnixtimestamp = (timestamp : any) : String =>{
    const date = new Date(timestamp * 1000);
    return date.toDateString()
  }