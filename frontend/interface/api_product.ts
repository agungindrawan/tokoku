import { IAddress } from "./address";
import { IHeaderResponse } from "./api_header";
import { StringOrUndefined } from "./types";

export interface IProductImage {
    id : string
    product_id : string
    image_url : string
}

export interface IProduct {
    id : string
    name : string
    product_category_id : string
    product_category_name : string
    warehouse_id : string,
    price : number
    stock : number
    status_name : string
    image_url : string
}

export interface ICartProduct {
    id : string
    product_id : string
    user_id : string
    qty : number
    product_name : string
    price : number
    image_url : SVGStringList
}

export interface IPaymentType {
    id : string
    name : string
    image_url : string
}


export interface IProductBuyer {
    id : string
    name : string
    product_category_id : string
    product_category_name : string
    warehouse_id : string,
    price : number
    stock : number
    status_name : string
    image_url : string
}

export interface ISubmitOrderRequest {
    address_id : string
    payment_type_id : string
}


export interface IGetListProductBuyerResponse {
    header : IHeaderResponse
    data : IProductBuyer[]
}


export interface IGetListProductSellerResponse {
    header : IHeaderResponse
    data : IProduct[]
}

export interface ISubmitOrderResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}

export interface IOrderItem {
    id: string;
    product_id: string;
    qty: number;
    product_name : string
    image_url: string;
    price: number;
}

export interface IOrder {
    id: string;
    user_id: string;
    address_id: string;
    address_name: string;
    total_price: number;
    ordered_at: string;
    payment_method_id: string;
    status_id: string;
    status_name: string;
    status_color: string;
    items : IOrderItem[]
}

export interface IGetListOrderResponse {
    header : IHeaderResponse
    data : IOrder[]
}


export interface IGetListPaymentTypeResponse {
    header : IHeaderResponse
    data : IPaymentType[]
}

export interface INullResponse {
    header : IHeaderResponse
    data : null
}

export interface IGetProductSellerByIDResponse {
    header : IHeaderResponse
    data : IProduct
}

export interface IGetProductBuyerByIDResponse {
    header : IHeaderResponse
    data : IProductBuyer
}

export interface IGetCartProductBuyerResponse {
    header : IHeaderResponse
    data : ICartProduct[]
}
export interface IGetCartProductByIdBuyerResponse {
    header : IHeaderResponse
    data : ICartProduct
}


export interface IProductCategory {
    id : string
    category_name : string
}


export interface IGetListProductCategoryResponse {
    header : IHeaderResponse
    data : IProductCategory[]
}

export interface ICreateProductResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}
export interface IAddCartProductResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}

export interface IUpdateCartProductResponse {
    header : IHeaderResponse
    data : null
}

export interface IDeleteCartProductResponse {
    header : IHeaderResponse
    data : null
}


export interface IDeleteProductResponse {
    header : IHeaderResponse
    data : null
}