import { IAddress } from "./address";
import { IHeaderResponse } from "./api_header";
import { StringOrUndefined } from "./types";

export interface IGetListAddressResponse {
    header : IHeaderResponse
    data : IAddress[]
}

export interface IGetListAddressRequest {
    token : StringOrUndefined
}

export interface ICreateAddressRequest {
    name : StringOrUndefined
    city_id : StringOrUndefined
    postal_code : StringOrUndefined
    token : StringOrUndefined
}

export interface ICreateAddressResponse {
    header : IHeaderResponse
    data: string
}

export interface IDeleteAddressResponse {
    header : IHeaderResponse
    data: null
}