import { StringOrUndefined } from "./types"

export interface IGetUser {
    full_name : StringOrUndefined
    email_address : StringOrUndefined
    phone_number : StringOrUndefined
    password : StringOrUndefined
}