import { IHeaderResponse} from "./api_header";

export interface IGetStoreResponse {
    header : IHeaderResponse
    data : {
        name : string
        points : number
    }
}

export interface ICreateStoreRequest {
    token : string | undefined
    name : string
}
export interface ICreateStoreResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}