import { IHeaderResponse } from "./api_header";

export interface ISignInResponse {
    header : IHeaderResponse
    data : string
}

export interface ISignInRequest {
    email : string
    password : string
}