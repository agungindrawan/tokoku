import { IHeaderResponse} from "./api_header";

export interface IWarehouse {
    id : string
    store_id : string
    address_id : string
    address_name : string
    name : string
}
export interface IGetWarehouseResponse {
    header : IHeaderResponse
    data : IWarehouse[]
}
export interface IGetWarehouseByIDResponse {
    header : IHeaderResponse
    data : IWarehouse
}

export interface ICreateWarehouseRequest {
    token : string | undefined
    name : string
    address_id : string
}
export interface ICreateWarehouseResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}

export interface IUpdateWarehouseRequest {
    id : string
    token : string | undefined
    name : string
    address_id : string
}
export interface ICreateWarehouseResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}

export interface IUpdateWarehouseResponse {
    header : IHeaderResponse
    data : {
        id : string
    }
}