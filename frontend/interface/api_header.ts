export interface IHeaderResponse {
    message : string,
    error : boolean
}