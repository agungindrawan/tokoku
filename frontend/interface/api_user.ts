import { IHeaderResponse} from "./api_header";

export interface IUserGetProfileResponse {
    header : IHeaderResponse
    data : {
        id : string
        full_name : string
        email_address : string
        phone_number : string
        password : string
    }
}

export interface IUserGetProfileRequest{
    token : string | undefined
}

export interface IUserUpdateRequest {
    full_name : string | undefined
    email_address : string | undefined
    phone_number : string | undefined
    password : string | undefined
    token : string | undefined
}

export interface IUserUpdateResponse {
    header : IHeaderResponse
    data : null
}