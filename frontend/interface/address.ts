
export interface IAddress {
    id : string
    name : string
    city : string
    postal_code : string
}