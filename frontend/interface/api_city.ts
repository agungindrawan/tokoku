import { IHeaderResponse } from "./api_header"
import { ICity } from "./city"

export interface IGetListCityResponse {
    header : IHeaderResponse
    data : ICity[]
}
