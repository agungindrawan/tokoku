"use client"
import { Container } from "postcss";
import { useState, useEffect, FormEvent } from "react";
import { getUserProfile, putUserUpdate } from "@/api/user";
import { IUserGetProfileRequest, IUserGetProfileResponse, IUserUpdateRequest } from "@/interface/api_user";
import Cookies from "js-cookie";
import { Toaster, toast } from 'sonner';


export default function ProfilePage() {
    const [data, setData] = useState({full_name : "", email_address : "", phone_number : "", password : ""})
    const token = Cookies.get('token')

    const fetchData = async () => {
        const request : IUserGetProfileRequest= {token : token}
        const responseGetUserProfile = await getUserProfile(request)
        if(responseGetUserProfile.header.error) {
            // setError(responseGetUserProfile.header.message)
            toast.error(responseGetUserProfile.header.message)
            return
        }
        setData(responseGetUserProfile.data)
    }

    const handleButtonSave = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const request : IUserUpdateRequest = {
            full_name : data?.full_name,
            email_address : data?.email_address,
            phone_number : data?.phone_number,
            password : data?.password,
            token : token
        }

        const updateUserResponse = await putUserUpdate(request)
        if(updateUserResponse.header.error) {
            toast.error(updateUserResponse.header.message)
        } else {
            toast.success("success update user")
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
       <>
        <Toaster richColors position="top-center"/>
           <div className="flex justify-between mt-2 items-center">
               <h1 className="text-2xl font-bold">Profile</h1>
               {/* <button className="bg-black text-white px-4 py-2 hover:bg-blue-600 rounded-lg">Back</button> */}
           </div>

           <form onSubmit={handleButtonSave} className="w-[60%] mt-8" action="#">
                  <div>
                      <label htmlFor="fullname" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Full Name</label>
                      <input onChange={(e) => setData({ ...data, full_name: e.target.value })} type="text" name="fullname" id="fullname" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" value={data?.full_name} required/>
                  </div>
                  <div>
                      <label htmlFor="email" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Email</label>
                      <input onChange={(e) => setData({ ...data, email_address: e.target.value })} type="email" name="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" value={data?.email_address} required/>
                  </div>
                  <div>
                      <label htmlFor="phone" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Phone</label>
                      <input  onChange={(e) => setData({ ...data, phone_number: e.target.value })}type="text" name="phone" id="phone" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="08912312312314" value={data?.phone_number}  required/>
                  </div>
                  <div className="mt-4">
                      <label htmlFor="password" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Password</label>
                      <input onChange={(e) => setData({ ...data, password: e.target.value })} type="password" name="password" id="password" placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" value={data?.password}/>
                  </div>
                  <button type="submit" className="w-1/3 text-white bg-black mt-10 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Save Change</button>    
              </form>
       </>
    );
  }
  