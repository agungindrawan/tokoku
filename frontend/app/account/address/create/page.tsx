'use client'
import Cookies from "js-cookie";
import { useState, useEffect, FormEvent } from "react";
import { ICity } from "@/interface/city";
import { IGetListCityResponse } from "@/interface/api_city";
import { getListCity } from "@/api/city";
import nProgress from "nprogress";
import { ICreateAddressRequest } from "@/interface/api_address";
import { postSaveAddress } from "@/api/address";
import { Toaster, toast } from "sonner";

export default function ProfilePage() {
    const token = Cookies.get('token')
    const [cities, setCities] = useState<ICity[]>([])

    const [formData, setFormData] = useState<ICreateAddressRequest>({name : "", city_id : "", postal_code : "", token : token})

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        nProgress.start()
        const request : ICreateAddressRequest = {
            name : formData?.name,
            city_id : formData?.city_id,
            postal_code : formData?.postal_code,
            token : formData?.token

        }
        const response = await postSaveAddress(request)
        if (response.header.error == false) {
            toast.success('successfully save address')
        }
        else {
            toast.error(response.header.message)
        }
        nProgress.done()
    }

    const fetchCitiies = async () => {
        nProgress.start()
        const responseAPI = await getListCity(token)
        if (responseAPI.header.error) {
            toast.error(responseAPI.header.message)
            nProgress.done()
            return
        }
        setCities(responseAPI.data)
        nProgress.done()
    }
    useEffect(() => {
        fetchCitiies()
    }, [])

    return (
        <>
            <Toaster position="top-center" richColors/>
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Address</h1>
                <a href="/account/address" className="bg-black text-white px-4 py-2 hover:bg-blue-600 rounded-lg">Back</a>
            </div>

            <form onSubmit = {handleSubmit} className="w-[60%] mt-8" action="#">
                <div>
                    <label htmlFor="address_name" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Address</label>
                    <input onChange={(e) => setFormData({ ...formData, name: e.target.value })} type="text" name="fullname" id="fullname" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required />
                </div>
                <div className="mt-4">
                    <label htmlFor="city" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select a City</label>
                    <select required onChange={(e) => setFormData({ ...formData, city_id: e.target.value })}  id="city" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected>Choose a city</option>
                        {
                            cities.length ? cities?.map((city) => (
                                <option value={city.id}>{city.city}</option>
                            )): <></>
                        }
                    </select>
                </div>
                <div>
                    <label htmlFor="postalcode" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Postal Code</label>
                    <input onChange={(e) => setFormData({ ...formData, postal_code: e.target.value })} type="text" name="phone" id="phone" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required />
                </div>

                <button type="submit" className="w-1/3 text-white bg-black mt-10 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-primary-300 text-sm font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Save Change</button>
            </form>
        </>
    );
}
