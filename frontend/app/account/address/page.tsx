'use client'
import { IGetListAddressRequest, IGetListAddressResponse } from "@/interface/api_address";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button } from "@nextui-org/react";
import { Container } from "postcss";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { deleteAddress, getListAddress } from "@/api/address";
import { Toaster, toast } from "sonner";
import { IAddress } from "@/interface/address";
import nProgress from "nprogress";
import { HouseLine, Mailbox, MapPin } from "@phosphor-icons/react";

export default function ProfilePage() {
    const token = Cookies.get('token')
    const [addresses, setAddresses] = useState<IAddress[]>([])

    const handleDeleteAddress = async (id: string) => {
        nProgress.start()
        const response = await deleteAddress(token, id)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }
        toast.success('successfully delete address')
        await fetchData()
        nProgress.done()
    }

    const fetchData = async () => {
        nProgress.start()
        const request: IGetListAddressRequest = { token: token }
        const getListAddressResponse = await getListAddress(request)
        if (getListAddressResponse.header.error) {
            toast.error(getListAddressResponse.header.message)
            nProgress.done()
            return
        }

        setAddresses(getListAddressResponse.data)
        nProgress.done()
    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <>
            <Toaster position="top-center" richColors />
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Address</h1>
                <a href="/account/address/create" className="bg-black text-white px-4 py-2  hover:bg-gray-600 rounded-lg">Create</a>
            </div>
            <div className="mt-10">

                <div className="grid grid-cols-2 gap-8">
                    {addresses ? addresses?.map((address, idx) => (
                        <div className="max-w-screen-sm p-6 bg-gray-50 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <div className="flex gap-3">
                                <HouseLine size={25} />
                                <p className="font-normal text-gray-700 dark:text-gray-400">{address.name}</p>
                            </div>
                            <div className="flex gap-3 mt-3">
                                <MapPin size={25} />
                                <p>{address.city}</p>
                            </div>
                            <div className="flex gap-3 mt-3 mb-3">
                                <Mailbox size={25} /> <p> {address.postal_code}</p>
                            </div>

                            {/* <a href="#" className="inline-flex mt-2 items-center px-4 py-2 hover:bg-gray-500 text-sm font-medium text-center text-white bg-black rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Edit
                            </a> */}
                            <button onClick={() => handleDeleteAddress(address.id)} className="inline-flex mt-2 items-center px-4 py-2 hover:bg-red-400 text-sm font-medium text-center text-white bg-red-500 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Delete
                            </button>
                        </div>
                    )
                    ) : <div>Address is not found</div>
                    }

                </div>

            </div>
        </>
    );
}
