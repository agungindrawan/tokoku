'use client'
import { IGetListAddressRequest, IGetListAddressResponse } from "@/interface/api_address";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button } from "@nextui-org/react";
import { Container } from "postcss";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { deleteAddress, getListAddress } from "@/api/address";
import { Toaster, toast } from "sonner";
import { IAddress } from "@/interface/address";
import nProgress from "nprogress";
import { Bag, HouseLine, Mailbox, MapPin, SquaresFour } from "@phosphor-icons/react";
import { IProduct } from "@/interface/api_product";
import { deleteProduct, getListProduct } from "@/api/product";
import Image from 'next/image'
import Link from "next/link";

export default function ProductPage() {
    const token = Cookies.get('token')
    const [products, setProducts] = useState<IProduct[]>([])

    const handleDeleteProduct = async (id: string) => {
        nProgress.start()
        const response = await deleteProduct(token, id)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }
        toast.success('successfully delete product')
        await fetchData()
        nProgress.done()
    }

    const fetchData = async () => {
        nProgress.start()
        const response = await getListProduct(token)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }

        setProducts(response.data)
        nProgress.done()
    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <>
            <Toaster position="top-center" richColors />
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Product</h1>
                <a href="/store/product/create" className="bg-black text-white px-4 py-2  hover:bg-gray-600 rounded-lg">Create</a>
            </div>
            <div className="mt-10">

                <div className="grid grid-cols-3 gap-3">
                    {products ? products?.map((product, idx) => (
                        <div className="max-w-screen-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <Image className='rounded-t-2xl h-56' width={500} height={500} style={{ width: '100%', height: 'auto' }} src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/images/${product.image_url}`} alt=''/>
                            <div className="flex gap-3 mt-4">
                            <Bag size={25} />
                                <p className="font-semibold text-gray-700 dark:text-gray-400">{product.name}</p>
                            </div>
                            <div className="flex gap-3 mt-3">
                            <SquaresFour size={25} />
                                <p className="font-semibold">{product.product_category_name}</p>
                            </div>
                            <div className="flex gap-2 mt-3 mb-3">
                                <span className="ml-1 font-semibold">Rp.</span> <p className="font-semibold"> {product.price}</p>
                            </div>

                            {/* <a href="#" className="inline-flex mt-2 items-center px-4 py-2 hover:bg-gray-500 text-sm font-medium text-center text-white bg-black rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Edit
                            </a> */}
                            <Link href={"/store/product/update/"+product.id} className="inline-flex mt-5 mr-2 items-center px-4 py-2 hover:bg-gray-500 text-sm font-medium text-center text-white bg-black rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Edit
                            </Link>
                            <button onClick={() => handleDeleteProduct(product.id)}  className="inline-flex mt-2 items-center px-4 py-2 hover:bg-red-400 text-sm font-medium text-center text-white bg-red-500 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Delete
                            </button>
                        </div>
                    )
                    ) : <div>Product is not found</div>
                    }

                </div>

            </div>
        </>
    );
}
