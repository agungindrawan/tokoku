'use client'
import Cookies from "js-cookie";
import React, { useState, useEffect, FormEvent } from "react";
import { ICity } from "@/interface/city";
import { IGetListCityResponse } from "@/interface/api_city";
import { getListCity } from "@/api/city";
import nProgress from "nprogress";
import { ICreateAddressRequest } from "@/interface/api_address";
import { postSaveAddress } from "@/api/address";
import { Toaster, toast } from "sonner";
import { IWarehouse } from "@/interface/api_warehouse";
import { getListWarehouse } from "@/api/warehouse";
import { IProduct, IProductCategory } from "@/interface/api_product";
import { getListProductCategory, getProductByID, postSaveProduct, putEditProduct } from "@/api/product";


interface ProductUpdateProps {
    params : {
        id : string
    }
}

export default function ProductPage({params} : ProductUpdateProps) {
    const {id} = params
    const token = Cookies.get('token')
    const [warehouses, setWarehouses] = useState<IWarehouse[]>([])
    const [categories, setCategories] = useState<IProductCategory[]>([])

    const [product, setProduct] = useState<IProduct>({id : id,name : "", product_category_name : "",product_category_id : "", warehouse_id : "", price : 0, stock : 0, status_name : "", image_url : ""})
    const inputFileRef = React.useRef<HTMLInputElement | null>(null);

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        nProgress.start()
        if (!inputFileRef.current?.files?.length) {
            alert('Please, select file you want to upload');
            nProgress.done()
            return;
        }

         /* Add files to FormData */
         const formData = new FormData();
         Object.values(inputFileRef.current.files).forEach(file => {
            formData.append('image', file);
        })
 
         formData.append('name', product.name)
         formData.append('product_category_id', product.product_category_id)
         formData.append('warehouse_id', product.warehouse_id)
         formData.append('price', String(product.price))
         formData.append('stock', String(product.stock))

        const response = await putEditProduct(formData, product.id, token)

        if (response.header.error == false) {
            toast.success('successfully save product')
        }
        else {
            toast.error(response.header.message)
        }
        nProgress.done()
    }

    const fetchWarehouses = async () => {
        nProgress.start()
        const response = await getListWarehouse(token)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }

        setWarehouses(response.data)
        nProgress.done()
    }

    
    const fetchCategories = async () => {
        nProgress.start()
        const response = await getListProductCategory(token)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }

        setCategories(response.data)
        nProgress.done()
    }
    const fetchProduct = async () => {
        nProgress.start()
        const response = await getProductByID(token, id)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }

        setProduct(response.data)
        nProgress.done()
    }


    useEffect(() => {
        fetchProduct()
        fetchWarehouses()
        fetchCategories()
    }, [])

    return (
        <>
            <Toaster position="top-center" richColors/>
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Product</h1>
                <a href="/store/product" className="bg-black text-white px-4 py-2 hover:bg-blue-600 rounded-lg">Back</a>
            </div>

            <form onSubmit={handleSubmit} className="w-[60%] mt-8" action="#">
                <div>
                    <label htmlFor="name" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Name</label>
                    <input onChange={(e) => setProduct({ ...product, name: e.target.value })} value={product.name}  type="text" name="fullname" id="name" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Kopi Aceh Gayo" required />
                </div>
                <div className="mt-4">
                    <label htmlFor="warehouse" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select a Warehouse</label>
                    <select required onChange={(e) => setProduct({ ...product, warehouse_id: e.target.value })}  value={product.warehouse_id}  id="warehouse" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected>Choose a Warehouse</option>
                        {
                            warehouses.length ? warehouses?.map((warehouse) => (
                                <option value={warehouse.id}>{warehouse.name}</option>
                            )): <></>
                        }
                    </select>
                </div>
                <div className="mt-4">
                    <label htmlFor="category" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select a Product Category</label>
                    <select required onChange={(e) => setProduct({ ...product, product_category_id: e.target.value })} value={product.product_category_id}   id="category" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected>Choose a Category</option>
                        {
                            categories.length ? categories?.map((category) => (
                                <option value={category.id}>{category.category_name}</option>
                            )): <></>
                        }
                    </select>
                </div>

                <div>
                    <label htmlFor="price" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Price</label>
                    <input onChange={(e) => setProduct({ ...product, price: Number(e.target.value) })} value={product.price} type="number" name="price" id="price" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="20000" required />
                </div>
                <div>
                    <label htmlFor="stock" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Stock</label>
                    <input onChange={(e) => setProduct({ ...product, stock: Number(e.target.value) })} value={product.stock}  type="number" name="stock" id="stock" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="10" required />
                </div>

                <div>
                <label className="block mb-2 mt-4 text-sm font-medium text-gray-900 dark:text-white" htmlFor="file_input">Upload Image</label>
                        <input type="file" ref={inputFileRef} className="block p-2 w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="file_input"/>
                </div>

                <button type="submit" className="w-1/3 text-white bg-black mt-10 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-primary-300 text-sm font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Save Change</button>
            </form>
        </>
    );
}
