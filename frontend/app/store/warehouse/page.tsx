'use client'
import { IGetListAddressRequest, IGetListAddressResponse } from "@/interface/api_address";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button } from "@nextui-org/react";
import { Container } from "postcss";
import { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { deleteAddress, getListAddress } from "@/api/address";
import { Toaster, toast } from "sonner";
import { IAddress } from "@/interface/address";
import nProgress from "nprogress";
import { HouseLine, Mailbox, MapPin } from "@phosphor-icons/react";
import {IWarehouse} from "@/interface/api_warehouse";
import {getListWarehouse} from "@/api/warehouse";

export default function WarehousePage() {
    const token = Cookies.get('token')
    const [warehouses, setWarehouses] = useState<IWarehouse[]>([])

    // const handleDeleteAddress = async (id: string) => {
    //     nProgress.start()
    //     const response = await deleteAddress(token, id)
    //     if (response.header.error) {
    //         toast.error(response.header.message)
    //         nProgress.done()
    //         return
    //     }
    //     toast.success('successfully delete address')
    //     await fetchData()
    //     nProgress.done()
    // }

    const fetchData = async () => {
        nProgress.start()
        const response = await getListWarehouse(token)
        if (response.header.error) {
            toast.error(response.header.message)
            nProgress.done()
            return
        }

        setWarehouses(response.data)
        nProgress.done()
    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <>
            <Toaster position="top-center" richColors />
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Warehouse</h1>
                <a href="/store/warehouse/create" className="bg-black text-white px-4 py-2  hover:bg-gray-600 rounded-lg">Create</a>
            </div>
            <div className="mt-10">

                <div className="grid grid-cols-2 gap-8">
                    {warehouses ? warehouses?.map((warehouse, idx) => (
                        <div className="max-w-screen-sm p-6 bg-gray-100 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <div className="flex gap-3">
                                <HouseLine size={25} />
                                <p className="font-normal text-gray-700 dark:text-gray-400">{warehouse.name}</p>
                            </div>
                            <div className="flex gap-3 mt-3">
                                <MapPin size={25} />
                                <p>{warehouse.address_name}</p>
                            </div>

                            <a href={"/store/product/update/"+warehouse.id} className="inline-flex mt-5 items-center px-4 py-2 hover:bg-gray-500 text-sm font-medium text-center text-white bg-black rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Edit
                            </a>
                            {/* <button className="inline-flex mt-2 items-center px-4 py-2 hover:bg-red-400 text-sm font-medium text-center text-white bg-red-500 rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                Delete
                            </button> */}
                        </div>
                    )
                    ) : <div>Address is not found</div>
                    }
                </div>

            </div>
        </>
    );
}
