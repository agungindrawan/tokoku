'use client'
import Cookies from "js-cookie";
import { useState, useEffect, FormEvent } from "react";
import { ICity } from "@/interface/city";
import { IGetListCityResponse } from "@/interface/api_city";
import { getListCity } from "@/api/city";
import nProgress from "nprogress";
import { ICreateAddressRequest, IGetListAddressRequest } from "@/interface/api_address";
import { getListAddress, postSaveAddress } from "@/api/address";
import { Toaster, toast } from "sonner";
import { IAddress } from "@/interface/address";
import { ICreateWarehouseRequest } from "@/interface/api_warehouse";
import { postSaveWarehouse } from "@/api/warehouse";

export default function ProfilePage() {
    const token = Cookies.get('token')
    const [addresses, setAddresses] = useState<IAddress[]>([])

    const [formData, setFormData] = useState<ICreateWarehouseRequest>({name : "", address_id : "", token : token})

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        nProgress.start()
        const request : ICreateWarehouseRequest = {
            name : formData?.name,
            address_id : formData?.address_id,
            token : formData?.token

        }
        const response = await postSaveWarehouse(request)
        if (response.header.error == false) {
            toast.success('successfully save warehouse')
        }
        else {
            toast.error(response.header.message)
        }
        nProgress.done()
    }

    const fetchAddresses = async () => {
        nProgress.start()
        const request : IGetListAddressRequest = {token : token}
        const responseAPI = await getListAddress(request)
        if (responseAPI.header.error) {
            toast.error(responseAPI.header.message)
            nProgress.done()
            return
        }
        setAddresses(responseAPI.data)
        nProgress.done()
    }
    useEffect(() => {
        fetchAddresses()
    }, [])

    return (
        <>
            <Toaster position="top-center" richColors/>
            <div className="flex justify-between items-center">
                <h1 className="text-2xl font-bold">Warehouse</h1>
                <a href="/store/warehouse" className="bg-black text-white px-4 py-2 hover:bg-blue-600 rounded-lg">Back</a>
            </div>

            <form onSubmit = {handleSubmit} className="w-[60%] mt-8" action="#">
                <div>
                    <label htmlFor="warehouse_name" className="block mt-4 mb-1 text-sm font-medium text-gray-900 dark:text-white">Warehouse Name</label>
                    <input onChange={(e) => setFormData({ ...formData, name: e.target.value })} type="text" id="warehouse_name" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Gudang Garam" required />
                </div>
                <div className="mt-4">
                    <label htmlFor="address_id" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select an Address</label>
                    <select required onChange={(e) => setFormData({ ...formData, address_id: e.target.value })}  id="address_id" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected>Choose an Address</option>
                        {
                            addresses.length ? addresses?.map((address) => (
                                <option value={address.id}>{address.name}</option>
                            )): <></>
                        }
                    </select>
                </div>
                <button type="submit" className="w-1/3 text-white bg-black mt-10 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-primary-300 text-sm font-medium rounded-lg px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Save Change</button>
            </form>
        </>
    );
}
