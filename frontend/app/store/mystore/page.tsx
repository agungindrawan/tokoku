"use client"
import { useState, useEffect, FormEvent } from "react";
import Cookies from "js-cookie";
import { Toaster, toast } from 'sonner';
import {getStore, postSaveStore} from "@/api/store";
import {ICreateStoreRequest} from "@/interface/api_store";


export default function MyStorePage() {
    const [data, setData] = useState({name : "", points : 0})
    const token = Cookies.get('token')

    const fetchData = async () => {
        const response = await getStore(token)
        if(response.header.error) {
            if(response.header.message == "store is not found") {
                toast.error("store hasn't been created yet")
                return
            }
            toast.error(response.header.message)
            return
        }
        setData(response.data)
    }

    const handleButtonSave = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const request : ICreateStoreRequest = {
            name : data?.name,
            token : token
        }

        const response = await postSaveStore(request)
        if(response.header.error) {
            toast.error(response.header.message)
        } else {
            toast.success("successfully updated store")
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
       <>
        <Toaster richColors position="top-center"/>
           <div className="flex justify-between mt-2 items-center">
               <h1 className="text-2xl font-bold">My Store</h1>
               {/* <button className="bg-black text-white px-4 py-2 hover:bg-blue-600 rounded-lg">Back</button> */}
           </div>

           <form onSubmit={handleButtonSave} className="w-[60%] mt-8" action="#">
                  <div>
                      <label htmlFor="fullname" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Store Name</label>
                      <input onChange={(e) => setData({ ...data, name: e.target.value })} type="text" name="fullname" id="fullname" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" value={data?.name} required/>
                  </div>
                  <div>
                      <label htmlFor="points" className="block mt-4 mb-1 font-medium text-sm text-gray-900 dark:text-white">Points</label>
                      <input type="number" name="points" id="points" className="bg-gray-50 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" value={data?.points} disabled/>
                  </div>
                  <button type="submit" className="w-1/3 text-white bg-black mt-10 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Save Change</button>    
              </form>
       </>
    );
  }
  