"use client";
import { IProductBuyer } from "@/interface/api_product";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";
import nProgress from "nprogress";
import { postCartProduct } from "@/api/product";
import { toast } from "sonner";

export type CardProductProps = {
  product: IProductBuyer;
};
const CardProduct = ({ product }: CardProductProps) => {
  const router = useRouter();
  return (
    <Link
      href={"/home/product/" + product.id}
      className="card card-compact bg-base-100 w-56 3xl:w-48 p-3 shadow-lg col-span-3 3xl:col-span-2"
    >
      <figure>
        <Image
          width={480}
          height={200}
          src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/images/${product.image_url}`}
          alt=""
        />
      </figure>
      <div className="card-body">
        <p className="text-left">{product.name}</p>
        <h2 className="font-bold text-lg text-left"> Rp {product.price}</h2>

        <div className="justify-end">
          <p className="font-bold text-left text-green-400">
            {product.product_category_name}
          </p>
        </div>
      </div>
    </Link>
  );
};
export default CardProduct;
