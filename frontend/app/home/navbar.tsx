export default function Navbar() {
    return (
        <div className="flex mt-20 gap-4 justify-between">
        <p className="text-3xl font-semibold mr-10">Explore</p>
        <button className="text-sm px-6 rounded-full font-semibold text-white bg-teal-500">
          All
        </button>
        <button className="px-6  rounded-full font-semibold bg-gray-200">
          Gaming
        </button>
        <button className="px-6  rounded-full font-semibold bg-gray-200">
          Makanan & Minuman
        </button>
        <form className="flex items-center 3xl:w-[450px] w-[400px]  3xl:ml-10 ">
          <div className="relative w-full">
            <input
              type="text"
              id="voice-search"
              className="bg-gray-100 border border-gray-200 text-sm rounded-full  w-full p-4 focus:ring-0 outline-none"
              placeholder="Search Product"
              required
            />
          </div>
          <button
            type="submit"
            className="inline-flex btn rounded-full justify-end items-center px-7 py-1 ml-2  text-sm font-medium text-white hover:bg-gray-500 bg-teal-500 ... focus:outline-none"
          >
            Search
          </button>
        </form>
      </div>
    )
}