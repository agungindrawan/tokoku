"use client";
import {
  getListProductBuyer,
  getProductBuyerByID,
  getProductByID,
  postCartProduct,
} from "@/api/product";
import { IProductBuyer } from "@/interface/api_product";
import Image from "next/image";
import nProgress from "nprogress";
import { useEffect, useState } from "react";
import { toast, Toaster } from "sonner";
import CardProduct from "../../card";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";
import { toRupiah } from "@/util/to_rupiah";
import { MapPinLine, Star } from "@phosphor-icons/react";

interface ProductDetailProps {
  params: {
    id: string;
  };
}
const ProductDetailPage = ({ params }: ProductDetailProps) => {
  const token = Cookies.get("token");
  const router = useRouter();
  const { id } = params;
  const [product, setProduct] = useState<IProductBuyer>({
    id: id,
    name: "",
    product_category_name: "",
    product_category_id: "",
    warehouse_id: "",
    price: 0,
    stock: 0,
    status_name: "",
    image_url: "",
  });
  const [products, setProducts] = useState<IProductBuyer[]>([]);

  const fetchData = async () => {
    nProgress.start();
    const response = await getListProductBuyer();
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }

    setProducts(response.data);
    nProgress.done();
  };
  const fetchProduct = async () => {
    nProgress.start();
    const response = await getProductBuyerByID(id);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }

    setProduct(response.data);
    nProgress.done();
  };

  const handleAddCartProduct = async (id: string) => {
    nProgress.start();
    const response = await postCartProduct(token, id, 1);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    toast.success("Produk telah ditambahkan di keranjang anda");
    nProgress.done();
  };

  const handleClickBeliSekarang = async (id: string) => {
    nProgress.start();
    const response = await postCartProduct(token, id, 1);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    router.push("/cart");

    nProgress.done();
  };

  useEffect(() => {
    fetchProduct();
    fetchData();
  }, []);
  return (
    <>
      <Toaster position="top-center" richColors />
      <div className="mt-10 grid ml-20 grid-cols-12">
        <Image
          className="col-span-3 rounded-lg"
          width={350}
          height={400}
          src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/images/${product.image_url}`}
          alt=""
        />
        <div className="col-span-5 ml-5">
          <h2 className="text-xl font-bold ">{product.name}</h2>
          <h2 className="mt-4 text-xl font-bold">{toRupiah(product.price)}</h2>
          <h2 className="mt-4 text-md">
            Kategori :{" "}
            <span className="font-semibold">
              {product.product_category_name}
            </span>
          </h2>
          <h2 className="text-md">
            Stok : <span className="font-semibold">{product.stock}</span>
          </h2>
          <button
            onClick={() => handleClickBeliSekarang(product.id)}
            className="py-4 px-5 rounded-full font-semibold text-white hover:bg-gray-500 bg-teal-500 mt-5"
          >
            Beli Sekarang
          </button>
          <button
            onClick={() => handleAddCartProduct(product.id)}
            className="py-4 px-5 rounded-full font-semibold bg-transparent hover:bg-gray-100 text-teal-500 border border-teal-500  mt-5 ml-2"
          >
            Tambah Ke Keranjang
          </button>
        </div>
        <div className="col-span-2 w-64 ml-10 p-3 rounded-xl  h-80 py-10 shadow-lg">
          <div className="flex gap-2 items-center">
            <Image
              className="h-10 w-10 mt-1 rounded-full"
              width={48}
              height={48}
              src={`https://www.static-src.com/wcsstore/Indraprastha/images/catalog/mlogo/APM-70329-ed3294ef-b3b6-469f-b304-542b68d6fd7b.jpeg?w=8`}
              alt=""
            />
            <h2 className="font-bold ">Samono Official Store</h2>
          </div>
          <h2 className="mt-5 flex gap-2  ml-1 font-light items-center">
            <Star className="" size={16}/>  <span className="">Rating Seller :</span>
            <span className="font-semibold text-sm">98%</span>
          </h2>
          <div className="flex mt-2 ml-1">
          <MapPinLine size={16} className="mt-1" />
            <div className=" ml-2">
              <h2 className="font-light">Lokasi Toko : </h2>
              <h2 className="text-sm font-semibold">
                Jalan Sultan Alauddin No. 12
              </h2>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-20">
        <hr />
        <h1 className="mt-5 text-lg font-bold ">Produk Lainnya di Toko ini</h1>
      </div>
      <div className="grid grid-cols-10  gap-4 justify-items-stretch mt-10 gap-y-8 justify-stretch">
        {products ? (
          products?.map((product, idx) => <CardProduct product={product} />)
        ) : (
          <></>
        )}
      </div>

      <div className="mt-12">
        <h1 className="mt-2 text-lg font-bold">Rekomendasi untuk anda</h1>
      </div>
      <div className="grid grid-cols-10  gap-4 justify-items-stretch mt-10 gap-y-8 justify-stretch">
        {products ? (
          products?.map((product, idx) => <CardProduct product={product} />)
        ) : (
          <></>
        )}
      </div>
    </>
  );
};

export default ProductDetailPage;
