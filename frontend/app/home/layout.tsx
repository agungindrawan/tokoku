import type {Metadata} from "next";
import {Inter} from "next/font/google";
import "../globals.css";
import Sidebar  from "@/components/sidebar/sidebar";
import Navbar from "./navbar";

const inter = Inter({subsets: ["latin"]});

export const metadata: Metadata = {
    title: "Dashboard | Sign In",
};

export default function AuthLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <div className="">
            {/* <Navbar/> */}
            <Sidebar />
            <div className="ml-52">
            <Navbar />
            {children}
            </div>
        </div>
    );
}
