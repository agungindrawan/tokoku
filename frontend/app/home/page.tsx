"use client";
import Navbar from "@/components/navbar/navbar";
import Image from "next/image";
import SmartphoneImage from "@/public/dashboard/smartphone.png";
import CardProduct from "./card";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import { IProductBuyer } from "@/interface/api_product";
import nProgress from "nprogress";
import { getListProductBuyer } from "@/api/product";
import { toast } from "sonner";

export default function HomePage() {
  const [products, setProducts] = useState<IProductBuyer[]>([]);

  const fetchData = async () => {
    nProgress.start();
    const response = await getListProductBuyer();
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }

    setProducts(response.data);
    nProgress.done();
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      {/* <div className="relative w-full mx-auto mt-10">
      
        <Image
          src={SmartphoneImage}
          className="w-full h-[450px] blur-sm object-cover relative"
          alt="smartphone"
        />

        <div className="absolute inset-0 bg-gray-700 opacity-60 rounded-md"></div>
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="flex flex-col items-center">
            <h2 className="text-white text-7xl font-bold mb-5">tokoKu</h2>
            <h2 className="text-white text-3xl font-light">
              Extra Diskon Up to 50% this week
            </h2>
            <button className="bg-black text-white hover:bg-blue-700 mt-10 px-8 py-4 rounded-lg font-bold">
              Shop Now
            </button>
          </div>
        </div>
      </div> */}


      {/* <div className="mt-10 flex gap-3">
        <button className="px-9 py-3 rounded-lg font-semibold bg-gradient-to-r text-white from-cyan-600 to-cyan-400 ...">
          Electronic
        </button>
        <button className="px-9 py-3 rounded-lg font-semibold bg-gradient-to-r text-white from-red-600 to-red-400 ...">
          Fashion
        </button>
        <button className="px-9 py-3 rounded-lg font-semibold bg-gradient-to-r text-white from-green-600 to-green-400 ...">
          Gaming
        </button>
        <button className="px-9 py-3 rounded-lg font-semibold bg-gradient-to-r text-white from-orange-600 to-orange-400 ...">
          Makanan & Minuman
        </button>
      </div> */}
      <div className="grid 3xl:grid-cols-10 grid-cols-12 mt-10 gap-y-8">
        {products ? (
          products?.map((product, idx) => <CardProduct product={product} />)
        ) : (
          <></>
        )}
      </div>
    </>
  );
}
