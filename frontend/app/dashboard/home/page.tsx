import Navbar from "@/components/navbar/navbar";
import Image from "next/image";
import SmartphoneImage from "@/public/dashboard/smartphone.png";
export default function HomePage() {
  return (
    <>
      <div className="relative w-full mx-auto mt-10">
        {/* <img className="h-64 w-full object-cover rounded-md" src="https://images.unsplash.com/photo-1680725779155-456faadefa26" alt="Random image"> */}
        <Image
          src={SmartphoneImage}
          className="w-full h-[550px] blur-sm object-cover relative"
          alt="smartphone"
        />

        <div className="absolute inset-0 bg-gray-700 opacity-60 rounded-md"></div>
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="flex flex-col items-center">
            <h2 className="text-white text-7xl font-bold mb-5">tokoKu</h2>
            <h2 className="text-white text-3xl font-light">
              Extra Diskon Up to 50% this week
            </h2>
            <button className="bg-black text-white hover:bg-blue-700 mt-10 px-8 py-4 rounded-lg font-bold">
              Shop Now
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
