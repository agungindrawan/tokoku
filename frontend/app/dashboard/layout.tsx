import type {Metadata} from "next";
import {Inter} from "next/font/google";
import "../globals.css";
import Navbar from "@/components/navbar/navbar";

const inter = Inter({subsets: ["latin"]});

export const metadata: Metadata = {
    title: "Dashboard | Sign In",
};

export default function AuthLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
        <body className={inter.className}>
        <div className="w-[70%] mx-auto">
            <Navbar/>
            {children}
        </div>
        </body>
        </html>
    );
}
