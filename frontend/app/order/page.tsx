"use client";
import {
  confirmedPayment,
  deleteCartProduct,
  getCartProduct,
  getListOrder,
  getListProductBuyer,
  getProductBuyerByID,
  getProductByID,
  postSubmitOrder,
  putEditProduct,
  updateCartProduct,
} from "@/api/product";
import {
  ICartProduct,
  IGetCartProductBuyerResponse,
  IOrder,
  IPaymentType,
  IProductBuyer,
  ISubmitOrderRequest,
} from "@/interface/api_product";
import { ArrowLeft, Check, Tornado, TrashSimple } from "@phosphor-icons/react";
import Image from "next/image";
import Link from "next/link";
import nProgress from "nprogress";
import { use, useEffect, useState } from "react";
import { toast, Toaster } from "sonner";
import Cookies from "js-cookie";
import { toRupiah } from "@/util/to_rupiah";
import { getAdapter } from "axios";
import { getDateUnixtimestamp } from "@/util/get_date";

const ProductDetailPage = () => {
  const token = Cookies.get("token");
  const [orders, setOrders] = useState<IOrder[]>([]);

  const fetchOrders = async () => {
    nProgress.start();
    const response = await getListOrder(token);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    setOrders(response.data);
    nProgress.done();
  };

  const handlePaymentConfirmed = async (orderID: string) => {
    nProgress.start();
    const response = await confirmedPayment(token, orderID)
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    toast.success("Pembayaran anda telah dikonfirmasi");
    fetchOrders()
    nProgress.done();
  };

  useEffect(() => {
    fetchOrders();
  }, []);
  return (
    <>
      <Toaster position="top-center" richColors />
      <div className="grid grid-cols-12 w-[70rem] mt-10">
        <div className="col-span-12">
          <div className="flex justify-between mr-2 mt-6">
            <p className="text-2xl font-bold pl-3 pr-8 py-4 text-white bg-teal-500 rounded-full rounded-l-none">
              Daftar Transaksi
            </p>
          </div>

          <div className="shadow-lg mt-10 p-4 border border-black border-opacity-10">
            <input
              type="text"
              placeholder="Cari transaksimu disini"
              className="input border border-gray-400 ml-3 mt-2 mr-2 ring-0 text-black rounded-full w-full max-w-xs"
            />
            <button className="px-5 py-3 bg-teal-500 text-white rounded-full">
              Cari
            </button>
            <div className="w-full mt-6 rounded">
              <button className="px-4 py-2 ml-2 text-white bg-teal-500 rounded-lg">
                Semua
              </button>
              <button className="px-4 py-2 ml-2 text-gray-600 border border-gray-400 rounded-lg">
                Berlangsung
              </button>
              <button className="px-4 py-2 ml-2 text-gray-600 border border-gray-400 rounded-lg">
                Berhasil
              </button>
              <button className="px-4 py-2 ml-2 text-gray-600 border border-gray-400 rounded-lg">
                Tidak Berhasil
              </button>
            </div>

            {orders ? (
              orders.map((order, idx) => (
                <div className="rounded-lg shadow-lg border border-gray-500 border-opacity-20 mx-2 my-4">
                  <div className="p-4 mb-4 flex items-center gap-3">
                    <p className="font-semibold">Belanja</p>
                    <p className="font-light">
                      {getDateUnixtimestamp(order.ordered_at)}
                    </p>
                    <p className={`font-semibold text-sm px-3 py-1 text-white rounded-full bg-cyan-500`}>
                      {order.status_name}
                    </p>
                    <p className="font-light">
                      IVR/20240630/XXIV/VI/2131287662
                    </p>
                  </div>
                  <div className="grid grid-cols-12">
                    <div className="col-span-6">
                      {order.items ? (
                        order.items.map((item, idx) => (
                          <div className="px-4 mb-4 flex items-start gap-3">
                            <Image
                              className="rounded-lg"
                              width={80}
                              height={200}
                              src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/images/${item.image_url}`}
                              alt=""
                            />
                            <div>
                              <p className="font-bold">{item.product_name}</p>
                              <p className="text-sm">
                                {item.qty} barang X {toRupiah(item.price)}
                              </p>
                            </div>
                          </div>
                        ))
                      ) : (
                        <></>
                      )}
                    </div>
                    <div className="col-span-4">
                      <div className="bg-teal-500 m-10 rounded-lg p-5">
                        <p className="text-white">
                          Total Belanja :{" "}
                          <span className="font-bold">
                            {" "}
                            {toRupiah(order.total_price)}
                          </span>{" "}
                        </p>
                      </div>
                      <button onClick={()=> handlePaymentConfirmed(order.id)} className="btn bg-green-500 m-10 text-white hover:bg-green-700">
                        Konfirmaasi Pembayaran
                      </button>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <></>
            )}
          </div>

          <div className="flex  justify-between my-10">
            <Link
              href="/home"
              className="text-white bg-teal-500 px-5 py-4 w-52 rounded-full font-bold flex gap-2 items-center"
            >
              <ArrowLeft size={22} /> Lanjutkan Belanja
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductDetailPage;
