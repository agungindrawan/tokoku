import type {Metadata} from "next";
import {Inter} from "next/font/google";
import "../globals.css";
import Sidebar  from "@/components/sidebar/sidebar";
const inter = Inter({subsets: ["latin"]});

export const metadata: Metadata = {
    title: "Dashboard | Sign In",
};

export default function AuthLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <div className="">
            {/* <Navbar/> */}
            <Sidebar />
            <div className="mx-auto 3xl:ml-40 3xl:w-[1180px] 2xl:ml-52 xl:ml-20  2xl:w-[1100px]">
            {children}
            </div>
        </div>
    );
}
