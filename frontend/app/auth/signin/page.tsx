"use client"
import React, { FormEvent, useState } from 'react'
import { useRouter } from 'next/navigation'
import { postSignIn } from '@/api/auth';
import Cookie from 'js-cookie';
import NextNProgress from 'nextjs-progressbar';
import nProgress from 'nprogress';
import Alert from '@/components/alert/alert';
import { Toaster, toast } from 'sonner';

export default function PageSignIn() {
    const [formData, setFormData] = useState({ email: "", password: "" });

    const router = useRouter();
    const handleRegistration = () => {
        router.push("/auth/signup")
    }
    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        nProgress.start()
        const responseLogin = await postSignIn(formData)
        if (responseLogin.header.error == false) {
            Cookie.set('token',responseLogin.data)
            router.push('/home');
        }
        else {
            toast.error(responseLogin.header.message)
            nProgress.done()
            return
        }
        nProgress.done()
        toast.success('success')
    }
    return (
        <>
        <NextNProgress />
        <Toaster richColors position="top-center"/>
        <div className="flex flex-col items-center justify-center h-screen">
                <h1 className="font-bold text-3xl">Sign in</h1>
                <p className="font-light mt-2">Enter your email and password to sign in this app</p>
                <form onSubmit={handleSubmit} className="w-[40%] mt-6" action="#">
                  <div>
                      <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                      <input onChange={(e) => setFormData({ ...formData, email: e.target.value })} type="email" name="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required/>
                  </div>
                  <div>
                      <label htmlFor="password" className="block mb-2 mt-4 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                      <input onChange={(e) => setFormData({ ...formData, password: e.target.value })} type="password" name="password" id="password" placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required/>
                  </div>

                  <button type="submit" className="w-1/2 flex justify-center mx-auto text-white bg-black mt-10 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Sign in</button>
                  <p className="flex mt-5 justify-center text-sm font-light text-gray-500 dark:text-gray-400">
                      Don’t have an account yet? <a href="#" className="font-medium ml-2 text-primary-600 hover:underline dark:text-primary-500">Sign up</a>
                  </p>
                
              </form>

        </div>
        </>
    );
  }
  