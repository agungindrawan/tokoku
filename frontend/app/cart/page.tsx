"use client";
import {
  deleteCartProduct,
  getCartProduct,
  getListProductBuyer,
  getProductBuyerByID,
  getProductByID,
  postSubmitOrder,
  putEditProduct,
  updateCartProduct,
} from "@/api/product";
import {
  ICartProduct,
  IGetCartProductBuyerResponse,
  IPaymentType,
  IProductBuyer,
  ISubmitOrderRequest,
} from "@/interface/api_product";
import { ArrowLeft, TrashSimple } from "@phosphor-icons/react";
import Image from "next/image";
import Link from "next/link";
import nProgress from "nprogress";
import { use, useEffect, useState } from "react";
import { toast, Toaster } from "sonner";
import Cookies from "js-cookie";
import { Trash } from "@phosphor-icons/react/dist/ssr";
import { toRupiah } from "@/util/to_rupiah";
import { IGetListAddressRequest } from "@/interface/api_address";
import { getListAddress } from "@/api/address";
import { IAddress } from "@/interface/address";
import { getListPaymentType } from "@/api/payment";
import { useRouter } from "next/navigation";

const ProductDetailPage = () => {
  const token = Cookies.get("token");
  const router = useRouter()
  const [cartProducts, setCartProducts] = useState<ICartProduct[]>([]);
  const [totalItem, setTotalItem] = useState<Number>(0);
  const [totalPrice, setTotalPrice] = useState<Number>(0);
  const [submitOrder, setSubmitOrder] = useState<ISubmitOrderRequest>({
    address_id: "",
    payment_type_id: "",
  });
  const [addresses, setAddresses] = useState<IAddress[]>([]);
  const [payments, setPayments] = useState<IPaymentType[]>([]);

  const fetchData = async () => {
    nProgress.start();
    const response = await getCartProduct(token);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    setCartProducts(response.data);
    setTotalItem(response.data?.length ?? 0);
    nProgress.done();
  };

  const fetchPayments = async () => {
    nProgress.start();
    const response = await getListPaymentType(token);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    setPayments(response.data);
    nProgress.done();
  };

  const fetchAddresses = async () => {
    nProgress.start();
    const request: IGetListAddressRequest = { token: token };
    const responseAPI = await getListAddress(request);
    if (responseAPI.header.error) {
      toast.error(responseAPI.header.message);
      nProgress.done();
      return;
    }
    setAddresses(responseAPI.data);
    nProgress.done();
  };

  const handleClickDeleteProduct = async (id: string) => {
    nProgress.start();
    const response = await deleteCartProduct(token, id);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }
    toast.success("Produk telah dihapus");
    fetchData();
    nProgress.done();
  };

  const handleOnChangeStock = (id: any, v: any) => {
    if (v > 0) {
      setCartProducts(
        cartProducts.map((product) =>
          product.id === id ? { ...product, qty: v } : { ...product }
        )
      );
    }
  };

  const handleButtonLanjutPembayaran = async () => {
    document.getElementById("my_modal_1")?.showModal()
    for (const product of cartProducts) {
      const response = await updateCartProduct(token, product.id, product.qty);
      if (response.header.error) {
        toast.error(response.header.message);
        nProgress.done();
        return;
      }
    }
    let totalPrice = 0;
    cartProducts.map((product) => (totalPrice += product.price * product.qty));
    setTotalPrice(totalPrice);

    nProgress.done();
  };

  const handleSubmitOrder = async () => {
    const response = await postSubmitOrder(token, submitOrder);
    if (response.header.error) {
      toast.error(response.header.message);
      nProgress.done();
      return;
    }

    toast.success("berhasil submit order produk");
    router.push("/order")
  };

  useEffect(() => {
    fetchData();
    fetchAddresses();
    fetchPayments();
  }, []);
  return (
    <>
      <Toaster position="top-center" richColors />
      <div className="grid grid-cols-12 mt-10">
        <div className="col-span-12">
          <div className="flex justify-between mr-2 mt-6">
            <p className="text-2xl font-bold bg-teal-500 rounded-full rounded-l-none text-white px-5 py-4">Keranjang Produk</p>
            <p className="text-2xl font-bold bg-teal-500 rounded-full text-white px-5 py-4">{String(totalItem)} Items</p>
          </div>
          <hr className="border mt-4 border-black border-opacity-35 mb-4" />

          <div className="w-full">
            <table className="table">
              {/* head */}
              <thead>
                <tr>
                  <th className="text-lg">Produk Detail</th>
                  <th className="text-lg">Jumlah</th>
                  <th className="text-lg">Harga</th>
                  <th className="text-lg">Total</th>
                </tr>
              </thead>
              <tbody>
                {/* row 1 */}
                {cartProducts ? (
                  cartProducts.map((product, idx) => (
                    <tr>
                      <td>
                        <div className="flex items-center gap-3">
                          <div className="avatar">
                            <div className="mask h-40 w-32">
                              <Image
                                className="rounded-lg"
                                width={180}
                                height={200}
                                src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/images/${product.image_url}`}
                                alt=""
                              />
                            </div>
                          </div>
                          <div>
                            <div className="font-bold text-lg">
                              {product.product_name}
                            </div>
                            <div className="text-sm">
                              <button
                                onClick={() =>
                                  handleClickDeleteProduct(product.id)
                                }
                                className="text-white bg-red-500 mt-2 ml-4 flex px-4 font-semibold rounded-full py-3"
                              >
                                Hapus
                              </button>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>
                        <input
                          type="number"
                          placeholder="10"
                          className="input text-lg text-white bg-teal-500 font-bold w-14 focus:border-none focus:ring-0"
                          defaultValue={product.qty}
                          value={product.qty}
                          onChange={(e) =>
                            handleOnChangeStock(product.id, e.target.value)
                          }
                        />
                      </td>
                      <td className="text-lg">{toRupiah(product.price)}</td>
                      <td className="text-lg"> {toRupiah(product.qty * product.price)}</td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </tbody>
              {/* foot */}
            </table>
          </div>
          <div className="flex  justify-between my-10">
            <Link
              href="/home"
              className="text-teal-500 rounded-full border border-teal-500 px-5 py-4 font-bold hover:text-gray-500 flex gap-2 items-center"
            >
              <ArrowLeft size={22} /> Lanjutkan Belanja
            </Link>
            <dialog id="my_modal_1" className="modal">
              <div className="modal-box">
                <form method="dialog">
                  {/* if there is a button in form, it will close the modal */}
                  <button className="bg-gray-200  font-bold btn-circle btn-ghost absolute right-3 top-3">
                    ✕
                  </button>
                </form>
                <h3 className="font-semibold text-lg">Ringkasan</h3>
                <div className="w-full font-light text-lg flex justify-between mt-5">
                  <p>Sub Total</p>
                  <p>{toRupiah(totalPrice)}</p>
                </div>
                <div className="w-full font-light text-lg flex justify-between">
                  <p>Biaya Ongkir</p>
                  <p>Free</p>
                </div>
                <hr className="border border-opacity-30 border-black mt-3" />
                <div className="w-full font-light text-lg flex justify-between mt-3">
                  <p>Total</p>
                  <p>{toRupiah(totalPrice)}</p>
                </div>
                <h3 className="font-semibold text-lg mt-4">
                  Alamat Pengiriman
                </h3>
                <select
                  required
                  onChange={(e) =>
                    setSubmitOrder({
                      ...submitOrder,
                      address_id: e.target.value,
                    })
                  }
                  id="address_id"
                  className="bg-gray-50 border mt-2 border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                >
                  <option selected>Pilih Alamat</option>
                  {addresses.length ? (
                    addresses?.map((address: any) => (
                      <option value={address.id}>{address.name}</option>
                    ))
                  ) : (
                    <></>
                  )}
                </select>
                <h3 className="font-semibold text-lg mt-4">Pembayaran</h3>
                <select
                  required
                  onChange={(e) =>
                    setSubmitOrder({
                      ...submitOrder,
                      payment_type_id: e.target.value,
                    })
                  }
                  id="payment_type_id"
                  className="bg-gray-50 border mt-2 border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                >
                <option selected>Pilih Pembayaran</option>
                  {payments.length ? (
                    payments?.map((payment: IPaymentType) => (
                      <option value={payment.id}>{payment.name}</option>
                    ))
                  ) : (
                    <></>
                  )}
                </select>
                <div className="modal-action">
                  <form method="dialog">
                    {/* if there is a button in form, it will close the modal */}
                    <button
                      onClick={handleSubmitOrder}
                      className="px-5 py-4 rounded-full font-bold bg-teal-500 text-white"
                    >
                      Submit Order
                    </button>
                  </form>
                </div>
              </div>
            </dialog>
            <button
              onClick={handleButtonLanjutPembayaran}
              className="bg-teal-500 rounded-full px-5 py-4 font-bold text-white hover:bg-gray-500 items-center"
            >
              Lanjut Pembayaran
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductDetailPage;
